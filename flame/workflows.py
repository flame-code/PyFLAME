import os 
import sys
import json 
import re
from random import sample 
from collections import defaultdict
from pymatgen.core.structure import Structure
from pymatgen.core.composition import Composition
from fireworks import Workflow
from flame.fireworks import *
from structure.core import *
from workflows.config import *

__author__ = "Sai Ram Kuchana and Hossein Mirhosseini"
__maintainer__ = "Hossein Mirhosseini"
__email__ = "mirhosse@mail.uni-paderborn.de"

def get_gensymcrys_wf(composition_list):
    wf_name = 'step-1'
    gensymcrys_fws = []
    for a_comp in composition_list:
        elements = []
        n_elmnt = []
        for e, n in Composition(a_comp).items():
            elements.append(str(e))
            n_elmnt.append(int(n))
        allowed_n_atom = get_allowed_n_atom_for_compositions([a_comp])
        for a_n_a in allowed_n_atom:
            gensymcrysfw = GensymcrysFW(elements, n_elmnt, a_n_a, name='gensymcrys_'+str(a_comp)+'_'+str(a_n_a)+'_atoms')
            gensymcrys_fws.append(gensymcrysfw)
    gensymcrys_wf = Workflow(gensymcrys_fws, name = wf_name)
    return gensymcrys_wf

def get_aver_dist_wf():
    wf_name = 'aver_dist'
    structures_dict = defaultdict(list)
    for root, dirs, taskfiles in os.walk(os.path.join(Flame_dir,'step-0','task_files')):
        for a_taskfile in taskfiles:
            with open(os.path.join(root,a_taskfile), 'r') as f:
                s = json.loads(f.read())
            if s["task_label"] == 'opt2':
                structure_from_task_input = Structure.from_dict(s['input']['structure'])
                structures_dict[len(structure_from_task_input.sites)].append(structure_from_task_input)
                structure_from_task_output = Structure.from_dict(s['output']['structure'])
                structures_dict[len(structure_from_task_output.sites)].append(structure_from_task_output)
            if s["task_label"] == 'bulk' or s["task_label"] == 'scaled_bulk':
                structure_from_task_output = Structure.from_dict(s['output']['structure'])
                structures_dict[len(structure_from_task_output.sites)].append(structure_from_task_output)
    aver_dist_fws = []
    for keys in structures_dict:
        averdistfw = AverDistFW(structures = structures_dict[keys], name = 'aver_dist_'+str(keys)+'_atoms')
        aver_dist_fws.append(averdistfw)
    aver_dist_wf = Workflow(aver_dist_fws, name = wf_name)
    return aver_dist_wf 

def get_train_wf(c_step_number):
    wf_name = 'flame_train_'+str(c_step_number)
    train_fws = []
    if input_list['number_of_epoch']:
        t_type = 'train_user_epoch'
    else:
        t_type = 'train_auto_epoch'
    for i in range(0,3):
        trainfw = TrainFW(step_number = c_step_number, name = 'flame_train', train_type = t_type)
        train_fws.append(trainfw)
    train_wf = Workflow(train_fws, name = wf_name)
    return train_wf 

def get_minhocao_wf(c_step_number):
    wf_name = 'flame_minhocao_'+str(c_step_number)
    file_name = os.path.join(random_structures_dir,'minhocao_seeds.json')
    with open(file_name, 'r') as f:
        structs  = json.loads(f.read())
    for i in range(1, int(re.split('-',c_step_number)[1])):
        step_n = 'step-'+str(i)
        f_path = os.path.join(Flame_dir,step_n,'minhocao')
        if os.path.exists(os.path.join(f_path,'next-step_bulk_structures.json')):
            with open(os.path.join(f_path,'next-step_bulk_structures.json'), 'r') as f:
                poslow_s = json.loads(f.read())
            structs.extend(poslow_s)
    selected_structs = sample(structs, input_list['bulk_minhocao']) if len(structs) > input_list['bulk_minhocao'] else structs
    minhocao_fws = []
    for struct_number, struct in enumerate(selected_structs):
        minhocaofw = MinhocaoFW(structure = Structure.from_dict(struct), step_number = c_step_number, name = 'flame_minhocao_struct-'+str(struct_number+1))
        minhocao_fws.append(minhocaofw)
    minhocao_wf = Workflow(minhocao_fws, name = wf_name)
    return minhocao_wf 

def get_minhocao_store_wf(c_step_number):
    wf_name = 'minhocao_store_'+str(c_step_number)
    minhocao_store_fw = []
    minhocaostorefw = MinhocaoStoreFW(step_number = c_step_number, name = 'minhocao_store-results')
    minhocao_store_fw.append(minhocaostorefw)
    minhocao_store_wf = Workflow(minhocao_store_fw, name = wf_name)
    return minhocao_store_wf

def get_minhopp_wf(c_step_number):
    wf_name = 'flame_minhopp_'+str(c_step_number)
    minhoppc_wf = None
    clusters = []
    file_name = os.path.join(random_structures_dir,'random_cluster_structures.json')
    with open(file_name, 'r') as f:
        cs = json.loads(f.read())
    for i in range(1, int(re.split('-',c_step_number)[1])):
        step_n = 'step-'+str(i)
        f_path = os.path.join(Flame_dir,step_n,'minhopp')
        if os.path.exists(os.path.join(f_path,'next-step_clusters.json')):
            with open(os.path.join(f_path,'next-step_clusters.json'), 'r') as f:
                poslow_c = json.loads(f.read())
            cs.extend(poslow_c)
    for a_cluster in cs:
        if len(Structure.from_dict(a_cluster).sites) in input_list['cluster_number_of_atoms']:
            clusters.append(a_cluster)
    minhoppc_fws = []
    selected_clusters = sample(clusters, input_list['cluster_minhopp']) if len(clusters) > input_list['cluster_minhopp'] else clusters 
    for struct_number, struct in enumerate(selected_clusters):
        minhoppcfw = MinhoppFW(structure = Structure.from_dict(struct), step_number = c_step_number, name = 'flame_minhopp_cluster-'+str(struct_number+1), job_type = 'cluster')
        minhoppc_fws.append(minhoppcfw)
    if len(minhoppc_fws) > 0:
        minhoppc_wf = Workflow(minhoppc_fws, name = wf_name)

    minhoppb_wf = None
    file_name = os.path.join(random_structures_dir,'minhopp_seeds.json')
    with open(file_name, 'r') as f:
        stressed_bulks = json.loads(f.read())
    minhoppb_fws = []
    selected_stressed_bulks = sample(stressed_bulks, input_list['bulk_minhopp']) if len(stressed_bulks) > input_list['bulk_minhopp'] else stressed_bulks
    for struct_number, struct in enumerate(selected_stressed_bulks):
        minhoppbfw = MinhoppFW(structure = Structure.from_dict(struct), step_number = c_step_number, name = 'flame_minhopp_bulk-'+str(struct_number+1), job_type = 'bulk')
        minhoppb_fws.append(minhoppbfw)
    if len(minhoppb_fws) > 0:
        minhoppb_wf = Workflow(minhoppb_fws, name = wf_name)
    return minhoppc_wf, minhoppb_wf

def get_minhopp_store_wf(c_step_number):
    wf_name = 'minhopp_store_'+str(c_step_number)
    minhopp_store_fw = []
    minhoppstorefw = MinhoppStoreFW(step_number = c_step_number, name = 'minhopp_store-results')
    minhopp_store_fw.append(minhoppstorefw)
    minhopp_store_wf = Workflow(minhopp_store_fw, name = wf_name)
    return minhopp_store_wf

def get_divcheck_b_wf(c_step_number):
    wf_name = 'flame_divcheck_'+str(c_step_number)
    divcheck_b_wf = None

    with open(os.path.join(Flame_dir,'aver_dist','aver_dist.json'), 'r') as f:
        aver_dist_dict = yaml.load(f, Loader=yaml.FullLoader)
    s_n = int(re.split('-',c_step_number)[1])

    with open(os.path.join(Flame_dir,c_step_number,'minhocao','nats.dat'), 'r') as f:
        lines = f.readlines()
    divcheck_b_fws = []
    for n_atom in lines:
        nat = n_atom.strip()
        dt = float(aver_dist_dict[str(nat)]) * float(input_list['dtol_prefactor'][s_n - 1])
        divcheckbfw = DivCheckbFW(number_of_atom = nat, dtol =  dt, step_number = c_step_number, name = 'flame_divcheck_'+str(nat)+'-atom-bulk')
        divcheck_b_fws.append(divcheckbfw)
    if len(divcheck_b_fws) > 0:
        divcheck_b_wf = Workflow(divcheck_b_fws, name = wf_name)
    return divcheck_b_wf

def get_divcheck_c_wf(c_step_number):
    divcheck_c_wf = None
    wf_name = 'flame_divcheck_'+str(c_step_number)

    with open(os.path.join(Flame_dir,'aver_dist','aver_dist.json'), 'r') as f:
        aver_dist_dict = yaml.load(f, Loader=yaml.FullLoader)
    s_n = int(re.split('-',c_step_number)[1])

    with open(os.path.join(Flame_dir,c_step_number,'minhopp','nats.dat'), 'r') as f:
        lines = f.readlines()
    divcheck_c_fws = []
    for n_atom in lines:
        nat = n_atom.strip()
        dt = float(aver_dist_dict[str(nat)]) * float(input_list['dtol_prefactor'][s_n - 1]) * input_list['prefactor_cluster']
        divcheckcfw = DivCheckcFW(number_of_atom = nat, dtol =  dt, step_number = c_step_number, name = 'flame_divcheck_'+str(nat)+'-atom-cluster')
        divcheck_c_fws.append(divcheckcfw)
    if len(divcheck_c_fws) > 0:
        divcheck_c_wf = Workflow(divcheck_c_fws, name = wf_name)
    return divcheck_c_wf

def get_flame_sp_wf(c_step_number, structures, j_type, wf_name):
    elmnt_list = read_element_list()
    flame_fws = []
    for a_structure in structures:
        flame_sp_fw = FLAMEsinglepointFW(step_number = c_step_number, structure = a_structure, elements = elmnt_list, job_type = j_type)
        flame_fws.append(flame_sp_fw)
    flame_sp_wf = Workflow(flame_fws, name = wf_name)
    return(flame_sp_wf) 
