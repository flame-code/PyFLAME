import os
import sys
import shutil
import re
import json
import math
import multiprocessing as mp
from datetime import datetime
from random import sample
from collections import defaultdict
from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar
from structure.core import read_element_list, is_structure_valid
from workflows.core import find_block_folder
from flame.core import *
from workflows.config import *

import flame.flame_functions.atoms
from flame.flame_functions.ascii import *
from flame.flame_functions.io_yaml import *
from flame.flame_functions.io_bin import *
from flame.flame_functions.vasp import *
from flame.flame_functions.latvec2dproj import *

def write_minhopp_files(struct, step_number, job_type):
    elmnt_list = read_element_list()

    Poscar(struct).write_file('posinp.vasp')
    atoms=poscar_read('posinp.vasp')
    atoms.units_length_io='angstrom'
    atoms.boundcond = 'bulk' if job_type == 'bulk' else 'free'
    atoms_out=[]
    atoms_out.append(Atoms())
    atoms_out[-1]=copy.copy(atoms)
    write_yaml(atoms_out,'posinp.yaml')

    write_FLAME_input_file('minhopp', elmnt_list, structure=struct, step_number=step_number)

    with open('input.minhopp', 'w') as f:
       f.write('             0 number of minima already found'+'\n')
       f.write('   0.01    0.001  0.1      ediff,ekin,dt'+'\n')

    for elmnt in elmnt_list:
        shutil.copyfile(os.path.join(Flame_dir,step_number,'train',str(elmnt)+'.ann.param.yaml'),\
                        os.path.join('./',str(elmnt)+'.ann.param.yaml'))

def store_minhopp_results(step_number):
    s_n = int(re.split('-',step_number)[1])
    min_d_prefactor = input_list['min_distance_prefactor'] * math.pow((1-float(input_list['descending_prefactor'])/100), s_n - 1) if input_list['descending_prefactor'] else input_list['min_distance_prefactor']

    all_poslow_bstructs, all_poslow_cstructs, all_traj_bstructs, all_traj_cstructs = read_minhopp_results(min_d_prefactor)

    with open(os.path.join(Flame_dir,step_number,'minhopp','poslows-bulk-'+step_number+'.json'), 'w') as f:
        json.dump(all_poslow_bstructs, f)
    with open(os.path.join(Flame_dir,step_number,'minhopp','poslows-cluster-'+step_number+'.json'), 'w') as f:
        json.dump(all_poslow_cstructs, f)
    with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-bulk-'+step_number+'.json'), 'w') as f:
        json.dump(all_traj_bstructs, f)
    with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-cluster-'+step_number+'.json'), 'w') as f:
        json.dump(all_traj_cstructs, f)

    nats = [keys for keys in all_traj_cstructs.keys()]
    with open(os.path.join(Flame_dir,step_number,'minhopp','nats.dat'), 'w') as f:
        for a_nat in nats:
            f.write(str(a_nat)+'\n')

def read_minhopp_results(min_d_prefactor):
    block_dir_list = find_block_folder()
    block_dir = block_dir_list[0]

    all_poslow_bstructs = defaultdict(list)
    all_poslow_cstructs = defaultdict(list) 
    all_traj_bstructs = defaultdict(list)
    all_traj_cstructs = defaultdict(list)

    bin_file_paths = []

    pool = mp.Pool(mp.cpu_count())

    for root, dirs, files in os.walk(block_dir):
        if 'posinp.yaml' in files:
            if 'poslow.yaml' in files:
                for a_file in files:
                    if 'mde' in a_file and a_file.endswith('bin'):
                        bin_file_paths.append(os.path.join(root,a_file))
                with open('monitoring.dat', 'a') as f:
                    f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S")+'\n'))
                    f.write('reading {}'.format(os.path.join(root,'poslow.yaml'))+'\n')
                try:
                    atoms_all_yaml = read_yaml(os.path.join(root,'poslow.yaml'))
                except:
                    continue
                ref_struct = Structure.from_file(os.path.join(root,'posinp.vasp'))
                n_atom = len(ref_struct.sites)

                for atoms in atoms_all_yaml:
                    conf = atoms2dict(atoms)
                    lattice = conf['conf']['cell']
                    crdnts = []
                    spcs = []
                    for coord in conf['conf']['coord']:
                        crdnts.append([coord[0],coord[1],coord[2]])
                        spcs.append(coord[3])
                    structure = Structure(lattice,spcs,crdnts,coords_are_cartesian=True)
                    if is_structure_valid(structure, min_d_prefactor, False, False):
                        if conf['conf']['bc'] == 'bulk':
                            all_poslow_bstructs[n_atom].append(conf)
                        if conf['conf']['bc'] == 'free':
                            all_poslow_cstructs[n_atom].append(conf)
            else:
                with open(log_file, 'a') as f:
                    f.write('no poslow.yaml file is found in {}'.format(root)+'\n')

    result_objects = [pool.apply_async(read_bin_file, args=(a_path, min_d_prefactor)) for a_path in bin_file_paths]

    for r_o in result_objects:
        confs = (r_o.get()[0])
        if confs:
            bc = r_o.get()[1]
            nat = r_o.get()[2]
            if bc == 'bulk':
                all_traj_bstructs[nat].extend(confs)
            if bc == 'free':
                all_traj_cstructs[nat].extend(confs) 

    pool.close()
    pool.join()

    return(all_poslow_bstructs, all_poslow_cstructs, all_traj_bstructs, all_traj_cstructs)

def read_bin_file(path, min_d_prefactor):
    bohr2ang=0.52917721
    traj_structs = []
    atoms_all_bin = []

    with open('monitoring.dat', 'a') as f:
        f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S")+'\n'))
        f.write('reading {}'.format(path)+'\n')
    try:
        atoms_all_bin = bin_read(path)
    except:
        return (None, None, None)

    if len(atoms_all_bin) < 1:
        return(None, None, None)

    atoms_bin_selected = sample(atoms_all_bin, 10) if len(atoms_all_bin) > 10 else atoms_all_bin

    bc =  atoms_bin_selected[0].boundcond
    nat = atoms_bin_selected[0].nat
    for atoms in atoms_bin_selected:
        atoms.cellvec[0][0]=round(atoms.cellvec[0][0]*bohr2ang, 10)
        atoms.cellvec[0][1]=round(atoms.cellvec[0][1]*bohr2ang, 10)
        atoms.cellvec[0][2]=round(atoms.cellvec[0][2]*bohr2ang, 10)
        atoms.cellvec[1][0]=round(atoms.cellvec[1][0]*bohr2ang, 10)
        atoms.cellvec[1][1]=round(atoms.cellvec[1][1]*bohr2ang, 10)
        atoms.cellvec[1][2]=round(atoms.cellvec[1][2]*bohr2ang, 10)
        atoms.cellvec[2][0]=round(atoms.cellvec[2][0]*bohr2ang, 10)
        atoms.cellvec[2][1]=round(atoms.cellvec[2][1]*bohr2ang, 10)
        atoms.cellvec[2][2]=round(atoms.cellvec[2][2]*bohr2ang, 10)
        for iat in range(atoms.nat):
            atoms.rat[iat][0]=atoms.rat[iat][0]*bohr2ang
            atoms.rat[iat][1]=atoms.rat[iat][1]*bohr2ang
            atoms.rat[iat][2]=atoms.rat[iat][2]*bohr2ang
        conf=atoms2dict(atoms)
        lattice = conf['conf']['cell']
        crdnts = []
        spcs = []
        for coord in conf['conf']['coord']:
            crdnts.append([coord[0],coord[1],coord[2]])
            spcs.append(coord[3])
        structure = Structure(lattice, spcs, crdnts, coords_are_cartesian = True)
        if is_structure_valid(structure, min_d_prefactor, False, False):
            traj_structs.append(conf)
    return(traj_structs, bc, nat)
