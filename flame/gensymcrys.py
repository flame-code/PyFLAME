import io
import os
import sys
import yaml
import json
from itertools import combinations_with_replacement
from collections import defaultdict
from pymatgen.core.periodic_table import Element
from pymatgen.core.structure import Structure
from pymatgen.analysis.molecule_structure_comparator import CovalentRadius
from structure.core import *
from workflows.core import find_block_folder
from workflows.config import *
import flame.flame_functions.atoms
from flame.flame_functions.io_yaml import *
from flame.flame_functions.vasp import *

def write_gensymcrys_files(elements, n_elmnt, a_n_a):
    input_file_path = os.path.join(run_dir,'flame_files') if input_list['user_specified_FLAME_files'] else os.path.join(PyFLAME_directory,'flame','flame_files')
    with open(os.path.join(input_file_path,'flame_in.yaml'), 'r') as f:
        flame_in = yaml.load(f, Loader=yaml.FullLoader)
    input_file = flame_in['gensymcrys']
    input_file['main']['types'] = ' '.join(elements)

    covalent_radius = CovalentRadius.radius
    vol = 0
    for i in range(len(elements)):
       vol += 8 * covalent_radius[elements[i]]**3 * n_elmnt[i]
    volperatom = vol/sum(n_elmnt)
    input_file['genconf']['volperatom_bounds'] = [volperatom, volperatom * 1.5]
    input_file['genconf']['nat_types_fu'] = n_elmnt
    input_file['genconf']['list_fu'] = [int(a_n_a/sum(n_elmnt))]
    rminpairs = []
    pairs = {}
    for a_pair in combinations_with_replacement(elements,2):
        pairs[''.join(a_pair)] = get_min_d(a_pair[0], a_pair[1], False) * input_list['min_distance_prefactor']
    input_file['genconf']['rmin_pairs'] = pairs

    with open('flame_in.yaml', 'w') as f:
        yaml.dump(input_file,f)

def store_gensymcrys_results():
    random_bulk_structures_dict = defaultdict(list)
    random_bulk_structures = []

    block_dir_list = find_block_folder()
    if block_dir_list:
        if len(block_dir_list) > 1:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
            exit()
        block_dir = block_dir_list[0]
    else:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed: calculation directory was not found <<<'+'\n')
        exit()

    atoms_all_yaml = []
    min_d_prefactor = input_list['min_distance_prefactor']
    for root, dirs, files in os.walk(os.path.join(output_dir,block_dir)):
        if 'posout.yaml' in files:
            try:
                atoms_all_yaml = read_yaml(os.path.join(root,'posout.yaml'))
            except:
                continue
            random_bulk_structures_n_atoms = []
            for atoms in atoms_all_yaml:
                conf = atoms2dict(atoms)
                lattice = conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                structure = Structure(lattice,spcs,crdnts,coords_are_cartesian=True)

                if input_list['check_density'] and os.path.exists(os.path.join(random_structures_dir,'densities.dat')):
                    with open(os.path.join(random_structures_dir,'densities.dat'), 'r') as f:
                        densitie = [line.strip() for line in f]
                    if structure.density.real < float(densitie[0]):
                        structure.scale_lattice(structure.volume * (structure.density.real/float(densitie[0])))
                    if structure.density.real > float(densitie[1]):
                        structure.scale_lattice(structure.volume * (structure.density.real/float(densitie[1])))

                if is_structure_valid(structure, min_d_prefactor, False, input_list['check_density']):
                    random_bulk_structures_n_atoms.append(structure)
            if len(random_bulk_structures_n_atoms) == 0:
                with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
                   f.write('>>> WARNING: no random structure for structures with {} atoms is generated <<<'.format(len(structure))+'\n')
            else:
                with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
                    f.write('{} random bulk structures with {} atoms are generated'.format(len(random_bulk_structures_n_atoms), len(structure))+'\n')
                random_bulk_structures.extend(random_bulk_structures_n_atoms)
    if len(random_bulk_structures) == 0:
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('>>> WARNING: No random bulk structure is generated <<<'+'\n')
    else:
        for a_random_bulk_structure in random_bulk_structures:
            random_bulk_structures_dict[len(a_random_bulk_structure.sites)].append(a_random_bulk_structure.as_dict())
        with open(os.path.join(random_structures_dir,'random_bulk_structures.json'),'w') as f:
            json.dump(random_bulk_structures_dict, f)
