import os
import subprocess
import shlex
from custodian import Custodian
from fireworks import explicit_serialize, FiretaskBase
from atomate.utils.utils import env_chk, get_logger
from flame.validators import *
from flame.handlers import * 
from flame.jobs import FlameJob


logger = get_logger(__name__)


@explicit_serialize
class RunFlameCustodian(FiretaskBase):
    required_params = ["flame_cmd"]
    optional_params = ["handler_group", "validator_group", "job_type"]

    def run_task(self, fw_spec):

        handler_groups = {
            "default": [FrozenFlameErrorHandler()],
            "train_auto_epoch": [FrozenFlameErrorHandler(), IncrreasingRMSEHandler()],
            "minhocao": [FrozenFlameErrorHandler(), IdleMinhocaoErrorHandler()],
            "minhopp":  [FrozenFlameErrorHandler(), IdleMinhoppErrorHandler()]
        }

        flame_cmd = env_chk(self["flame_cmd"], fw_spec)
        jobs = [FlameJob(flame_cmd = flame_cmd)]
        job_type = self.get("job_type", "normal")
        if job_type == "normal":
            handlers = handler_groups["default"]
        elif job_type == "train_auto_epoch":
            handlers = handler_groups[job_type]
        elif job_type == "train_user_epoch":
            handlers = handler_groups["default"]
        elif job_type == "minhocao":
            handlers = handler_groups["minhocao"]
        elif job_type == "minhopp":
            handlers = handler_groups["minhopp"]
        else:
            raise ValueError(f"Unsupported job type: {job_type}")
        c = Custodian(handlers, jobs, validators=[FlameFilesValidator()])
        c.run()
