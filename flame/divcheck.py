import os
import sys
import shutil
import re
import json
import yaml
from yaml import Loader 
from random import sample
from pymatgen.core.structure import Structure
from structure.core import read_element_list
from flame.core import *
from workflows.config import *

import flame.flame_functions.atoms
from flame.flame_functions.ascii import *
from flame.flame_functions.io_yaml import *
from flame.flame_functions.vasp import *
from flame.flame_functions.latvec2dproj import *

def write_divcheck_b_files(step_number, nat):
    s_n = int(re.split('-',step_number)[1])
    poslow_structures = []
    energy_list = []
    if s_n > 1:
        p_step_path = os.path.join(Flame_dir,'step-'+str(s_n - 1))

        if os.path.exists(os.path.join(p_step_path,'minhocao','poslows-'+'step-'+str(s_n - 1)+'.json')):
            with open(os.path.join(p_step_path,'minhocao','poslows-'+'step-'+str(s_n - 1)+'.json'), 'r') as f:
                poslows_confs = json.loads(f.read())
            if nat in poslows_confs.keys():
                for a_conf in poslows_confs[nat]:
                    lattice = a_conf['conf']['cell']
                    crdnts = []
                    spcs = []
                    for coord in a_conf['conf']['coord']:
                        crdnts.append([coord[0],coord[1],coord[2]])
                        spcs.append(coord[3])
                    poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                    energy_list.append(-1) # it is NOT a new structure
            elif nat in input_list['reference_number_of_atoms']:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no poslow bulk structure with {} atoms (generated with minhocao) from {} is available <<<'.format(nat, str(s_n - 1))+'\n')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow bulk structure (generated with minhocao) from {} <<<'.format('step-'+str(s_n - 1))+'\n')  

        if os.path.exists(os.path.join(p_step_path,'minhopp','poslows-bulk-'+'step-'+str(s_n - 1)+'.json')):
            with open(os.path.join(p_step_path,'minhopp','poslows-bulk-'+'step-'+str(s_n - 1)+'.json'), 'r') as f:
                poslows_confs = json.loads(f.read())
            if nat in poslows_confs.keys():
                for a_conf in poslows_confs[nat]:
                    lattice = a_conf['conf']['cell']
                    crdnts = []
                    spcs = []
                    for coord in a_conf['conf']['coord']:
                        crdnts.append([coord[0],coord[1],coord[2]])
                        spcs.append(coord[3])
                    poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                    energy_list.append(-1) # it is NOT a new structure
            elif nat in input_list['reference_number_of_atoms']:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no poslow bulk structure with {} atoms (generated with minhopp) from {} is available <<<'.format(nat, str(s_n - 1))+'\n')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow bulk structure (generated with minhopp) from {} <<<'.format('step-'+str(s_n - 1))+'\n')

    if os.path.exists(os.path.join(Flame_dir,step_number,'minhopp','poslows-bulk-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhopp','poslows-bulk-'+step_number+'.json'), 'r') as f:
            poslows_confs = json.loads(f.read())
        if nat in poslows_confs.keys():
            for a_conf in poslows_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                energy_list.append(1) # it is a new structure        
        elif nat in input_list['reference_number_of_atoms']:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow bulk structure with {} atoms (generated with minhopp) is available <<<'.format(nat)+'\n')
    else:           
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no poslow bulk structure (generated with minhopp) from {} <<<'.format(step_number)+'\n')

    if os.path.exists(os.path.join(Flame_dir,step_number,'minhocao','poslows-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhocao','poslows-'+step_number+'.json'), 'r') as f:
            poslows_confs = json.loads(f.read())
        if nat in poslows_confs.keys():
            for a_conf in poslows_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                energy_list.append(1) # it is a new structure
        elif nat in input_list['reference_number_of_atoms']:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow bulk structure with {} atoms (generated with minhocao) is available <<<'.format(nat)+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no poslow bulk structure (generated with minhocao) from {} <<<'.format(step_number)+'\n')

    structure_list = []
    if os.path.exists(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json'), 'r') as f:
            minhocao_confs = json.loads(f.read())
        if nat in minhocao_confs.keys():
            for a_conf in minhocao_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                structure_list.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
        elif nat in input_list['reference_number_of_atoms']:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no bulk structure with {} atoms (generated with minhocao) is available <<<'.format(nat)+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no  bulk structure (generated with minhocao) from {} <<<'.format(step_number)+'\n')

    if os.path.exists(os.path.join(Flame_dir,step_number,'minhopp','minhopp-bulk-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-bulk-'+step_number+'.json'), 'r') as f:
            minhopp_bulk_confs = json.loads(f.read())
        if nat in minhopp_bulk_confs.keys():
            for a_conf in minhopp_bulk_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                structure_list.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
        elif nat in input_list['reference_number_of_atoms']:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no bulk structure with {} atoms (generated with minhopp) is available <<<'.format(nat)+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no  bulk structure (generated with minhopp) from {} <<<'.format(step_number)+'\n')

    selected_structures = sample(structure_list, 10000-len(poslow_structures)) if len(structure_list) >= 10000-len(poslow_structures) else structure_list
    all_structures = poslow_structures + selected_structures
    energy_list = energy_list + (10000-len(poslow_structures)) * [1] # new structures
    bc_list = len(all_structures) * ['bulk']
    write_p_f_from_list(all_structures, bc_list, energy_list, False, 'position_force_divcheck.yaml')

    elmnt_list = read_element_list()
    write_SE_ann_input(elmnt_list)
    write_FLAME_input_file('divcheck', elmnt_list)
    with open('list_posinp_check.yaml', 'w') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_divcheck.yaml'+'\n')
    with open('nat.dat', 'w') as f:
        f.write(nat)

def write_divcheck_c_files(step_number, nat):
    s_n = int(re.split('-',step_number)[1])
    poslow_structures = []
    energy_list = []
    if s_n > 1:
        p_step_path  = os.path.join(Flame_dir,'step-'+str(s_n - 1))

        if os.path.exists(os.path.join(p_step_path,'minhopp','poslows-cluster-'+'step-'+str(s_n - 1)+'.json')):
            with open(os.path.join(p_step_path,'minhopp','poslows-cluster-'+'step-'+str(s_n - 1)+'.json'), 'r') as f:
                poslows_confs = json.loads(f.read())
            if nat in poslows_confs.keys():
                for a_conf in poslows_confs[nat]:
                    lattice = a_conf['conf']['cell']
                    crdnts = []
                    spcs = []
                    for coord in a_conf['conf']['coord']:
                        crdnts.append([coord[0],coord[1],coord[2]])
                        spcs.append(coord[3])
                    poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                    energy_list.append(-1) # it is NOT a new structure
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no poslow cluster structure with {} atoms from {} is available <<<'.format(nat, p_step_path)+'\n')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow clustter structure from {} <<<'.format('step-'+str(s_n - 1))+'\n')

    if os.path.exists(os.path.join(Flame_dir,step_number,'minhopp','poslows-cluster-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhopp','poslows-cluster-'+step_number+'.json'), 'r') as f:
            poslow_confs = json.loads(f.read())
        if nat in poslow_confs.keys():
            for a_conf in poslow_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                poslow_structures.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
                energy_list.append(1) # it is a new structure
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no poslow cluster structure with {} atoms from {} is available <<<'.format(nat, step_number)+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no poslow clustter structure from {} <<<'.format(step_number)+'\n')

    structure_list = []
    if os.path.exists(os.path.join(Flame_dir,step_number,'minhopp','minhopp-cluster-'+step_number+'.json')):
        with open(os.path.join(Flame_dir,step_number,'minhopp','minhopp-cluster-'+step_number+'.json'), 'r') as f:
            minhocao_confs = json.loads(f.read())
        if nat in minhocao_confs.keys():
            for a_conf in minhocao_confs[nat]:
                lattice = a_conf['conf']['cell']
                crdnts = []
                spcs = []
                for coord in a_conf['conf']['coord']:
                    crdnts.append([coord[0],coord[1],coord[2]])
                    spcs.append(coord[3])
                structure_list.append(Structure(lattice, spcs, crdnts, coords_are_cartesian=True).as_dict())
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no cluster structure with {} atoms is available <<<'.format(nat)+'\n')
    else:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no clustter structure from {} <<<'.format(step_number)+'\n')

    selected_structures = sample(structure_list, 10000-len(poslow_structures)) if len(structure_list) >= 10000-len(poslow_structures) else structure_list
    all_structures = poslow_structures + selected_structures
    energy_list = energy_list + (10000-len(poslow_structures)) * [1] # new structures
    bc_list = len(all_structures) * ['free']

    write_p_f_from_list(all_structures, bc_list, energy_list, False, 'position_force_divcheck.yaml')
 
    elmnt_list = read_element_list()
    write_SE_ann_input(elmnt_list)
    write_FLAME_input_file('divcheck', elmnt_list)
    with open('list_posinp_check.yaml', 'w') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_divcheck.yaml'+'\n')
    with open('nat.dat', 'w') as f:
        f.write(nat)

def run_pickdiff(step_number, nat, dtol):

    if dtol == 0:
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: no dtol is provided for structures with {} atoms <<<'.format(nat)+'\n')
    else:
        if os.path.exists('distall'):
            dist=[]
            f=open('distall','r')
            nn=-1
            nconf=0
            for line in f.readlines():
                if not int(line.split()[1])==nn:
                    nconf+=1
                    dist.append([])
                    for iconf in range(nconf-1):
                        dist[-1].append(dist[iconf][nconf-1])
                    dist[-1].append(0.0)
                    nn=int(line.split()[1])
                dist[-1].append(float(line.split()[4]))
            f.close()
            nconf+=1
            dist.append([])
            for iconf in range(nconf-1):
                dist[-1].append(dist[iconf][nconf-1])
            dist[-1].append(0.0)

            sel=[]
            for iconf in range(nconf):
                if iconf==0:
                    sel.append(0) #This is the first configuration
                    continue
                new=True
                for jconf in range(len(sel)):
                    if dist[iconf][sel[jconf]]<dtol:
                        new=False
                        break
                if new==True:
                    sel.append(iconf)
            atoms_all=read_yaml('position_force_divcheck.yaml')
            atoms_all_out=[]
            for iconf in range(len(sel)):
                atoms_all_out.append(Atoms)
                atoms_all_out[-1]=copy.deepcopy(atoms_all[sel[iconf]])
            atoms_all_yaml = []
            json_dump = []
            for atoms in atoms_all_out:
                dict_atoms=atoms2dict(atoms)
                if dict_atoms['conf']['epot'] > 0: # only new structures
                    atoms_all_yaml.append(atoms)
                    json_dump.append(dict_atoms)
            with open('checked_position_force.json', 'w') as f:
                json.dump(json_dump, f)

            write_yaml(atoms_all_yaml, 'checked_position_force.yaml')
        else:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no distall in {} <<<'.format(os.getcwd())+'\n')
