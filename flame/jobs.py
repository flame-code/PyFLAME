import subprocess
import os
import logging

from custodian.custodian import Job

logger = logging.getLogger(__name__)

class FlameJob(Job):
    def __init__(
        self,
        flame_cmd,
        output_file="flame.out",
        stderr_file="flame.err"
    ):
        self.flame_cmd = flame_cmd
        self.output_file = output_file
        self.stderr_file = stderr_file
        
    def setup(self):
        pass
    
    def run(self):
        cmd = self.flame_cmd
        logger.info("Running {}".format(" ".join(cmd)))
        with open(self.output_file, "w") as f_std, open(self.stderr_file, "w", buffering=1) as f_err:
            return subprocess.Popen(cmd, stdout=f_std, stderr=f_err, shell = True, universal_newlines=True)

    def postprocess(self):
        pass
        
