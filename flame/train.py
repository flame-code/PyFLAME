import os
import yaml
import json
import re
import stat
import math
import shutil
from random import sample
from collections import defaultdict
from pymatgen.core.structure import Structure 
from structure.core import read_element_list
from workflows.core import find_block_folder
from flame.core import *
from workflows.config import *

def write_train_files(step_number):
    with open(os.path.join(Flame_dir,step_number,'train','position_force_train_all.json'), 'r') as f:
        training_set = json.loads(f.read())
    indices_list = list(range(len(training_set)))
    valid_indices = sample(indices_list, int(len(indices_list)/10))
    for rem in valid_indices:
        indices_list.remove(rem)
    train_indices = indices_list

    f_name ='position_force_train_valid.yaml'
    valid_structure_list = []
    valid_energy_list = []
    valid_force_list = []
    valid_bc_list = []
    for i in valid_indices:
        valid_structure_list.append(training_set[i]['structure'])
        valid_energy_list.append(training_set[i]['energy'])
        valid_force_list.append(training_set[i]['forces'])
        valid_bc_list.append(training_set[i]['bc'])
    write_p_f_from_list(valid_structure_list, valid_bc_list, valid_energy_list, valid_force_list, f_name)
    with open('list_posinp_valid.yaml', 'a') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_train_valid.yaml'+'\n')
    with open('list_posinp_train.yaml', 'a') as f:
        f.write('files:'+'\n')
    t = int(len(indices_list)/10000)
    samp = int(len(indices_list)/(t+1))
    for i in range(1, t+2):
        f_name = 'position_force_train_train_'+'t'+str(i).zfill(3)+'.yaml'
        train_indices_t = sample(train_indices,samp)
        train_structure_list = []
        train_energy_list = []
        train_force_list = []
        train_bc_list = []

        for rem in train_indices_t:
            train_structure_list.append(training_set[rem]['structure'])
            train_energy_list.append(training_set[rem]['energy'])
            train_force_list.append(training_set[rem]['forces'])
            train_bc_list.append(training_set[rem]['bc'])
            train_indices.remove(rem)
        write_p_f_from_list(train_structure_list, train_bc_list, train_energy_list, train_force_list, f_name)
        with open('list_posinp_train.yaml', 'a') as f:
            f.write(' - {}'.format('position_force_train_train_'+'t'+str(i).zfill(3)+'.yaml'+'\n'))
    elmnt_list = read_element_list()
    write_SE_ann_input(elmnt_list, step_number)
    write_FLAME_input_file('train', elmnt_list, nconf_rmse = len(valid_structure_list))

def select_a_train(step_number):
    number_of_epoch = input_list['number_of_epoch']
    trains = defaultdict(list)
    elmnt_list = read_element_list()

    block_dir_list = find_block_folder()
    if block_dir_list:
        if len(block_dir_list) > 1:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
            exit()
        block_dir = block_dir_list[0]
    else:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed: calculation directory was not found <<<'+'\n')
        exit()

    train_number = 0
    for root, dirs, files in os.walk(block_dir):
        if 'train_output.yaml' in files:
            valid_rmse = [] 
            with open(os.path.join(root,'train_output.yaml'), 'r') as f:
                t_o = yaml.load(f, Loader=yaml.FullLoader)
            for i in range(len(t_o['training iterations'])):
                try:
                    valid_rmse.append(t_o['training iterations'][i]['valid']['rmse'])
                except:
                    pass
            trains[root].extend(valid_rmse)
            train_number = train_number + 1
            shutil.copyfile(os.path.join(root,'train_output.yaml'),\
                            os.path.join(Flame_dir,step_number,'train','train_number_'+str(train_number)+'_train_output.yaml'))
            if number_of_epoch:
                for elmnt in elmnt_list:
                    try:
                        shutil.copyfile(os.path.join(root,str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)),\
                                        os.path.join(Flame_dir,step_number,'train','train_number_'+str(train_number)+'_'+str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)))
                    except:
                        with open(log_file, 'a') as f:
                            f.write('>>> ERROR: no ann potential for epoch {} was found <<<'.format(number_of_epoch)+'\n')
                        exit()

            else:
                n_epoch = valid_rmse.index(min(valid_rmse))
                for elmnt in elmnt_list:
                    shutil.copyfile(os.path.join(root,str(elmnt)+'.ann.param.yaml.'+str(n_epoch).zfill(5)),\
                                    os.path.join(Flame_dir,step_number,'train','train_number_'+str(train_number)+'_'+str(elmnt)+'.ann.param.yaml'+str(n_epoch).zfill(5)))

    rmse_min = 100
    tpath = False
    if number_of_epoch:
        for keys in trains:
            try:
                if trains[keys][number_of_epoch] < rmse_min:
                    rmse_min = trains[keys][number_of_epoch]
                    tpath = keys
            except IndexError:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no RMSE for epoch {} was found <<<'.format(number_of_epoch)+'\n')
                continue       
        if tpath:
            for elmnt in elmnt_list:
                shutil.copyfile(os.path.join(tpath,str(elmnt)+'.ann.param.yaml.'+str(number_of_epoch).zfill(5)),\
                                os.path.join(Flame_dir,step_number,'train',str(elmnt)+'.ann.param.yaml'))
            shutil.copyfile(os.path.join(tpath,'train_output.yaml'), os.path.join(Flame_dir,step_number,'train','train_output.yaml'))
    else:
        for keys in trains:
            if min(trains[keys]) < rmse_min:
                rmse_min = min(trains[keys])
                tpath = keys
                n_epoch =  trains[keys].index(rmse_min)
        for elmnt in elmnt_list:
            shutil.copyfile(os.path.join(tpath,str(elmnt)+'.ann.param.yaml.'+str(n_epoch).zfill(5)),\
                            os.path.join(Flame_dir,step_number,'train',str(elmnt)+'.ann.param.yaml'))
        shutil.copyfile(os.path.join(tpath,'train_output.yaml'), os.path.join(Flame_dir,step_number,'train','train_output.yaml'))

def collect_training_data(step_number):
    training_set = [] 
    di = {}
    s_n = int(re.split('-',step_number)[1])
    p_step_path  = os.path.join(Flame_dir,'step-'+str(s_n - 1))
    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
        min_epa = float(f.readline().strip())
    for root, dirs, files in os.walk(os.path.join(p_step_path,'task_files')):
        for a_taskfile in files:
            with open(os.path.join(root,a_taskfile), 'r') as f:
                s = json.loads(f.read())
            if float(s['output']['energy_per_atom']) < min_epa + 1.5:
                di['structure'] = s['output']['structure']
                di['forces']    = s['output']['forces']
                di['energy']    = s['output']['energy']
                if 'cluster' in s['task_label']:
                    di['bc'] = 'free'
                elif 'bulk' in s['task_label'] or 'opt2' in s['task_label']:
                    di['bc'] = 'bulk'
                else:
                    continue
                training_set.append(di)
                di = {}
            if len(s['calcs_reversed'][0]['output']['ionic_steps']) == 1:
                continue
            maxmin_force = [[1.00,0.90],[0.90,0.80],[0.80,0.70],[0.70,0.60],[0.60,0.50],\
                            [0.50,0.48],[0.48,0.46],[0.46,0.44],[0.44,0.42],[0.42,0.40],\
                            [0.40,0.38],[0.38,0.36],[0.36,0.34],[0.34,0.32],[0.32,0.30],\
                            [0.30,0.28],[0.28,0.26],[0.26,0.24],[0.24,0.22],[0.22,0.20],\
                            [0.20,0.18],[0.18,0.16],[0.16,0.14],[0.14,0.12],[0.12,0.10],\
                            [0.10,0.09],[0.09,0.08],[0.08,0.07],[0.07,0.06],[0.06,0.05]]
            found = len(maxmin_force) * [False]

            for ionic_step in range(len(s['calcs_reversed'][0]['output']['ionic_steps'])-1, 0, -1):
                this_epot = float(s['calcs_reversed'][0]['output']['ionic_steps'][ionic_step]['e_wo_entrp'])
                nsites = s['calcs_reversed'][0]['nsites']
                this_epa = this_epot/nsites
                if this_epa < min_epa + 1.5:
                    this_forces = s['calcs_reversed'][0]['output']['ionic_steps'][ionic_step]['forces']
                    this_tot_forces = []
                    for a_force in range(0,len(this_forces)):
                        this_tot_forces.append(math.sqrt(this_forces[a_force][0]**2 + this_forces[a_force][1]**2 + this_forces[a_force][2]**2))
                    max_this_tot_foce = max(this_tot_forces)
                    for f in range(len(maxmin_force)):
                        if not found[f] and max_this_tot_foce < maxmin_force[f][0] and max_this_tot_foce >= maxmin_force[f][1]:
                            di['structure'] = s['calcs_reversed'][0]['output']['ionic_steps'][ionic_step]['structure']
                            di['forces'] = this_forces
                            di['energy'] = this_epot
                            if 'cluster' in s['task_label']:
                                di['bc'] = 'free'
                            elif 'bulk' in s['task_label'] or 'opt2' in s['task_label']:
                                di['bc'] = 'bulk'
                            else:
                                continue
                            training_set.append(di)
                            di = {}
                            found[f] = True
                            break
    if s_n > 1:
        try:
            with open(os.path.join(p_step_path,'train','position_force_train_all.json'), 'r') as f:
                f_s=json.loads(f.read())
            training_set.extend(f_s)
        except:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no training data from the previous step <<<'+'\n')           
    with open(os.path.join(Flame_dir,step_number,'train','position_force_train_all.json'), 'w') as f:
        json.dump(training_set, f)
    with open(os.path.join(Flame_dir,step_number,'train','nat_epa.dat'), 'w') as f:
        for i in range(len(training_set)):
            nat = len(Structure.from_dict(training_set[i]['structure']).sites)
            epot = training_set[i]['energy'] 
            epa = epot/nat
            f.write('{} {}'.format(str(nat), str(epa))+'\n')

