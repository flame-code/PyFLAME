import os
import sys
import yaml
from pymatgen.io.vasp import Poscar
from structure.core import read_element_list
from flame.core import *
from workflows.config import *

import flame.flame_functions.atoms
from flame.flame_functions.ascii import *
from flame.flame_functions.io_yaml import *
from flame.flame_functions.vasp import *
from flame.flame_functions.latvec2dproj import *

def write_single_point_files(structure, job_type):
    elmnt_list = read_element_list()
    write_FLAME_input_file('single_point', elmnt_list)
    Poscar(structure).write_file('posinp.vasp')
    atoms=poscar_read('posinp.vasp')
    atoms.units_length_io='angstrom'
    for i in range(len(atoms.bemoved)):
        atoms.bemoved[i] = 'FFF'
    if 'bulk' in job_type:
        atoms.boundcond='bulk'
    if 'cluster' in job_type:
        atoms.boundcond='free'
    atoms_out=[]
    atoms_out.append(Atoms())
    atoms_out[-1]=copy.copy(atoms)
    write_yaml(atoms_out,'posinp.yaml')

def write_energy():
    atoms_all_yaml=read_yaml('./posout.yaml')
    conf = atoms2dict(atoms_all_yaml[0])
    epot = conf['conf']['epot']
    with open('./energies.dat', 'a') as f:
        f.write('{}'.format(epot)+'\n')
