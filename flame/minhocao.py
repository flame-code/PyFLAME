import os
import sys
import shutil
import re
import json
import math
import multiprocessing as mp
from datetime import datetime
from random import sample
from collections import defaultdict
from pymatgen.core.structure import Structure
from pymatgen.io.vasp import Poscar
from structure.core import read_element_list, is_structure_valid
from workflows.core import find_block_folder
from flame.core import *
from workflows.config import *

import flame.flame_functions.atoms
from flame.flame_functions.ascii import *
from flame.flame_functions.io_yaml import *
from flame.flame_functions.vasp import *
from flame.flame_functions.latvec2dproj import *

def write_minhocao_files(struct, step_number):
    elmnt_list = read_element_list()

    Poscar(struct).write_file('poscur.vasp')
    atoms=poscar_read('poscur.vasp')
    atoms.cellvec,atoms.rat=latvec2dproj(atoms.cellvec,atoms.rat,atoms.nat)
    ascii_write(atoms, 'poscur.ascii')

    write_FLAME_input_file('minhocao', elmnt_list, structure=struct, step_number=step_number)

    s_n = int(re.split('-',step_number)[1])
    with open('ioput', 'w') as f:
       f.write('  0.01        {}       {}         ediff, temperature, maximal temperature'\
              .format(input_list['minhocao_temp'][s_n - 1],input_list['minhocao_max_temp'][s_n - 1])+'\n')

    with open('earr.dat', 'w') as f:
        f.write('  0         {}          # No. of minima already found, no. of minima to be found in consecutive run'\
               .format(input_list['minhocao_n_min'][s_n - 1])+'\n')
        f.write('  0.400000E-03  0.150000E+00  # delta_enthalpy, delta_fingerprint')

    for elmnt in elmnt_list:
        shutil.copyfile(os.path.join(Flame_dir,step_number,'train',str(elmnt)+'.ann.param.yaml'),\
                        os.path.join('./',str(elmnt)+'.ann.param.yaml'))

def store_minhocao_results(step_number):
    s_n = int(re.split('-',step_number)[1])
    min_d_prefactor = input_list['min_distance_prefactor'] * math.pow((1-float(input_list['descending_prefactor'])/100), s_n - 1) if input_list['descending_prefactor'] else input_list['min_distance_prefactor']

    all_poslow_structs, all_minhocao_structs = read_minhocao_results(min_d_prefactor)

    with open(os.path.join(Flame_dir,step_number,'minhocao','poslows-'+step_number+'.json'), 'w') as f:
        json.dump(all_poslow_structs, f)
    with open(os.path.join(Flame_dir,step_number,'minhocao','minhocao-'+step_number+'.json'), 'w') as f:
        json.dump(all_minhocao_structs, f)

    nats = [keys for keys in all_minhocao_structs.keys()]
    with open(os.path.join(Flame_dir,step_number,'minhocao','nats.dat'), 'w') as f:
        for a_nat in nats:
            f.write(str(a_nat)+'\n')

def read_minhocao_results(min_d_prefactor):
    block_dir_list = find_block_folder()
    block_dir = block_dir_list[0]

    all_poslow_structs = defaultdict(list)
    all_minhocao_structs = defaultdict(list)

    all_posmd_file_paths = []

    pool = mp.Pool(mp.cpu_count())

    for root, dirs in walklevel(block_dir, 1):
        for dirname in dirs:
            if os.path.exists(os.path.join(root,dirname,'poscur.ascii')):
                if os.path.exists(os.path.join(root,dirname,'global.mon')):
                    for a_file in os.listdir(os.path.join(root,dirname)):
                        if 'poslow' in a_file and a_file.endswith('ascii'):
                            with open('monitoring.dat', 'a') as f:
                                f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S")+'\n'))
                                f.write('reading {}'.format(os.path.join(root,dirname,a_file))+'\n')
                            try:
                                atoms = ascii_read(os.path.join(root,dirname,a_file))
                            except:
                                continue
                            conf=atoms2dict(atoms)
                            if conf['conf']['coord'] or conf['conf']['nat'] != 0:
                                lattice = conf['conf']['cell']
                                crdnts = []
                                spcs = []
                                for coord in conf['conf']['coord']:
                                    crdnts.append([coord[0],coord[1],coord[2]])
                                    spcs.append(coord[3])
                                structure = Structure(lattice,spcs,crdnts,coords_are_cartesian=True)
                                nat = len(structure.sites)
                                if is_structure_valid(structure, min_d_prefactor, False, False):
                                    all_poslow_structs[nat].append(conf)

                    with open(os.path.join(root,dirname,'global.mon'), 'r') as f:
                        lines = f.readlines()[1:]
                    for foldertitle in ('T', 'F', 'S'):
                        folder_paths = []
                        for l in lines:
                            f_i = str(re.split('\s+',l)[1]).zfill(5)
                            if re.split('\s+',l)[11] == foldertitle:
                                folder_paths.append(os.path.join(root,dirname,'data_hop_'+f_i))
                        folder_paths_selected = sample(folder_paths, 20) if len(folder_paths) > 20 else folder_paths
                        posmd_file_paths = []
                        for folderpath in folder_paths_selected:
                            for a_file in os.listdir(folderpath):
                                if 'posmd' in a_file and a_file.endswith('ascii'):
                                    posmd_file_paths.append(os.path.join(folderpath,a_file))

                        posmd_file_paths_selected = sample(posmd_file_paths, 150) if len(posmd_file_paths) > 150 else posmd_file_paths
                        all_posmd_file_paths.extend(posmd_file_paths_selected)
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no global.mon file is found in {} <<<'.format(os.path.join(root,dirname))+'\n')

    result_objects = [pool.apply_async(read_ascii_file, args=(a_path, min_d_prefactor)) for a_path in all_posmd_file_paths]

    for r_o in result_objects:
        conf = (r_o.get()[0])
        if conf:
            nat = r_o.get()[1]
            all_minhocao_structs[nat].append(conf)

    pool.close()
    pool.join()

    return(all_poslow_structs, all_minhocao_structs)

def read_ascii_file(path, min_d_prefactor):
    minhocao_structs = []
    with open('monitoring.dat', 'a') as f:
        f.write('time: {}'.format(datetime.now().strftime("%H:%M:%S")+'\n'))
        f.write('reading {}'.format(path)+'\n')
    try:
        atoms=ascii_read(path)
    except:
        return (None, None)

    conf=atoms2dict(atoms)
    lattice = conf['conf']['cell']
    crdnts = []
    spcs = []
    for coord in conf['conf']['coord']:
        crdnts.append([coord[0],coord[1],coord[2]])
        spcs.append(coord[3])
    structure = Structure(lattice, spcs, crdnts, coords_are_cartesian=True)
    nat = len(structure.sites)
    if is_structure_valid(structure, min_d_prefactor, False, False):
        return (conf, nat)
    else:
        return (None, None)

def walklevel(path, depth):
    base_depth = path.rstrip(os.path.sep).count(os.path.sep)
    for root, dirs, files in os.walk(path):
        yield root, dirs
        cur_depth = root.count(os.path.sep)
        if base_depth + depth <= cur_depth:
            del dirs[:]
