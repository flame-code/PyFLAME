import os 
from custodian.custodian import ErrorHandler
import time 
import yaml 
from workflows.config import *

class FrozenFlameErrorHandler(ErrorHandler):
    """
    Check if FLAME log file is updated
    """
    is_monitor = True

    def __init__(self, output_filename="flame_log.yaml", timeout=21600):

        self.output_filename = output_filename
        self.timeout = timeout

    def check(self):
        """
        Check for error.
        """
        st = os.stat(self.output_filename)
        if time.time() - st.st_mtime > self.timeout:
            return True
        else:
            return False

    def correct(self):
        return {"errors": ["Frozen FLAME job"], "actions": None}

class IncrreasingRMSEHandler(ErrorHandler):
    """
    Check if rmse is increasing 
    """
    is_monitor = True

    def __init__(self, output_filename="train_output.yaml"):
        self.output_filename = output_filename

    def check(self):
        """
        Check for error.
        """
        valid_rmse = []
        t_o = {}
        t_o['training iterations'] = None

        try:
            if os.stat(self.output_filename).st_size != 0:
                with open(self.output_filename, 'r') as f:
                    t_o = yaml.load(f, Loader=yaml.FullLoader)
        except:
            pass

        if t_o['training iterations']:
            for i in range(len(t_o['training iterations'])):
                try:
                    valid_rmse.append(t_o['training iterations'][i]['valid']['rmse'])
                except:
                    pass
        if len(valid_rmse) > 0:
            rmse_min = min(valid_rmse)
            if rmse_min < valid_rmse[-1] and len(t_o['training iterations'])>11:
                return True
            else:
               return False

    def correct(self):
        """
        No correction is needed. Just stop training.
        """
        return {"errors": ["Terminated train job"], "actions": None}

class IdleMinhocaoErrorHandler(ErrorHandler):
    """
    Check if minhocao job is producing data
    """
    is_monitor = True

    def __init__(self, output_filename="global.mon"):

        self.output_filename = output_filename

    def check(self):
        """
        Check for error.
        """

        with open('flame_in.yaml', 'r') as f:
            flame_in = yaml.load(f, Loader=yaml.FullLoader)
        timeout = float(flame_in['main']['time_limit']) * 3600

        try:
            st = os.stat(self.output_filename)
        except:
            st = os.stat('poscur.vasp')

        if time.time() - st.st_mtime > timeout:
            return True
        else:
            return False

    def correct(self):
        return {"errors": ["Idle minhocap job"], "actions": None}

class IdleMinhoppErrorHandler(ErrorHandler):
    """
    Check if minhopp job is producing data
    """
    is_monitor = True

    def __init__(self, output_filename="monminhopp/monitoring.000"):

        self.output_filename = output_filename

    def check(self):
        """
        Check for error.
        """
        with open('flame_in.yaml', 'r') as f:
            flame_in = yaml.load(f, Loader=yaml.FullLoader)
        timeout = float(flame_in['main']['time_limit']) * 3600

        try:
            st = os.stat(self.output_filename)
        except:
            st = os.stat('posinp.vasp')

        if time.time() - st.st_mtime > timeout:
            return True
        else:
            return False

    def correct(self):
        return {"errors": ["Idle minhopp job"], "actions": None}

