import io
import os
import sys
import json
import yaml
import re
from pymatgen.core.structure import Structure
from pymatgen.core.periodic_table import Element
from structure.core import read_element_list
from workflows.core import find_block_folder
from flame.core import *
from workflows.config import *

def write_aver_dist_files(structures):
    structure_list = []
    energy_list = []
    bc_list = []
    for i in range(len(structures)):
        structure_list.append(structures[i].as_dict())
        energy_list.append(0.01)
        bc_list.append('bulk')
   
    write_p_f_from_list(structure_list, bc_list, energy_list, False, 'position_force_divcheck.yaml')

    elmnt_list = read_element_list()
    write_SE_ann_input(elmnt_list)    
    write_FLAME_input_file('aver_dist', elmnt_list)
    with open('list_posinp_check.yaml', 'w') as f:
        f.write('files:'+'\n')
        f.write(' - position_force_divcheck.yaml'+'\n')
    nat = str(len(structures[0].sites)) 
    with open('nat.dat', 'w') as f:
        f.write(nat)

def compute_aver_dist():
    d = {}
    nat_list = []

    block_dir_list = find_block_folder()
    if block_dir_list:
        if len(block_dir_list) > 1:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
            exit()
        block_dir = block_dir_list[0]
    else:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed: calculation directory was not found <<<'+'\n')
        exit()

    for root, dirs, files in os.walk(os.path.join(output_dir,block_dir)):
        if 'position_force_divcheck.yaml' in files:
            with open(os.path.join(root,'nat.dat'), 'r') as f:
                nat = f.readline().strip()
            if nat not in nat_list:
                nat_list.append(nat)
            if 'distall' in files:
                if os.path.getsize(os.path.join(root,'distall')):
                    dis_list = []
                    with open(os.path.join(root,'distall'), 'r') as f:
                        for line in f:
                            dis_list.append(float(re.split('\s+', line)[4]))
                    d[nat] = (sum(dis_list)/len(dis_list))
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: distall file in {} is empty <<<'.format(root)+'\n')
            else: 
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no distall in {} <<<'.format(root)+'\n')
    for nl in nat_list:
        try:
            if d[nl]:
                pass
        except KeyError:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no average distance fot structures with {} atoms <<< '.format(nl)+'\n')
            d[nl] = [0]

    with open(os.path.join(Flame_dir,'aver_dist','aver_dist.json'), 'w') as f:
        json.dump(d, f)
