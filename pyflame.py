#!/usr/bin/env python
import os
import sys
from workflows.config_set import config_set

__author__ = "Hossein Mirhosseini"
__copyright__ = ""
__maintainer__ = "Hossein Mirhosseini"
__email__ = "mirhosse@mail.uni-paderborn.de"

if __name__ == "__main__":

    run_dir = os.getcwd()
    if os.path.exists(os.path.join(run_dir,'input.yaml')) and os.path.exists(os.path.join(run_dir,'restart.yaml')) and os.path.exists(os.path.join(run_dir,'config.yaml')):
        pass
    else:
        print('>>> ERROR: the run directory is empty. input.yaml, restart.yaml, and config.yaml are needed for PyFLAME. <<<')
        sys.exit()

    config_set(run_dir)

    from workflows.main import main
    main()
