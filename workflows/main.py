import os
import sys
from workflows.config import *

def main():
    stpnmbr = restart['re-start_from_step']
    if stpnmbr == 0:
        from workflows.step0 import step_0
        if step_0():
            stpnmbr = 1
    if stpnmbr == 1:
        from workflows.step1 import step_1
        if step_1():
            stpnmbr = 2
    if stpnmbr == 2:
        from workflows.step2 import step_2
        if step_2():
            stpnmbr = 3
    if stpnmbr == 3:
        from workflows.step3 import step_3
        if step_3():
            stpnmbr = 4
    if stpnmbr == 4:
        from workflows.step4 import step_4
        step_4()
    if stpnmbr == -1:
        from workflows.core import rerun
        rerun()
