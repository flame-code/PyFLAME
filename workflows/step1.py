import os
from sys import exit
from datetime import datetime
from flame.workflows import get_gensymcrys_wf
from flame.gensymcrys import store_gensymcrys_results
from structure.core import *
from structure.workflows import get_pyxtal_wf
from workflows.core import *
from workflows.config import *

def step_1():
    with open(log_file, 'a') as f:
        f.write('STEP 1'+'\n')
        f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

    block_dir_list = find_block_folder()
    if block_dir_list:
        if len(block_dir_list) > 1:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
            exit()
        block_dir = block_dir_list[0]
        if len(os.listdir(block_dir)) != 0:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
            exit()

    if input_list['random_structure_generation'] != 0:
        # get compositions and elements 
        composition_list = read_composition_list() 
        if not composition_list:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: no composition is provided <<<'+'\n')
            exit()
        # get workflow
        if input_list['random_structure_generation'] == 1:
            with open(log_file, 'a') as f:
                f.write('random structure generation with gensymcrys'+'\n')
            step1_wf = get_gensymcrys_wf(composition_list)

        if input_list['random_structure_generation'] == 2:
            with open(log_file, 'a') as f:
                f.write('random structure generation with PyXtal'+'\n')
            step1_wf = get_pyxtal_wf(composition_list)

        with open(log_file, 'a') as f:
            f.write('For more details see {}/random_structure.dat'.format(random_structures_dir)+'\n')

        # add wf to lp
        add_wf(step1_wf)
        # run
        if not run_jobs('random_struct_gen', output_dir):
            with open(log_file, 'a') as f:
                f.write('>>> Cannot run jobs for random structure generation <<<'+'\n')
            exit()
        # wait until all jobs are done
        while True:
            fizzle_lostruns(3600)
            if run_exists():
                sleep(60)
            else:
                break
        # check launchpad state
        lp_state = check_lp(['step-1'])
        if lp_state == 'FIZZLED':
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: random structure generation workflow is FIZZLED <<<'+'\n')
        if lp_state == 'unknown':
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
            exit()

        # store random structures
        if input_list['random_structure_generation'] == 1:
            store_gensymcrys_results()
        # mv/rm calculation files
        clean_after_run()
    else:
        with open(log_file, 'a') as f:
            f.write('Nothing to do!'+'\n') 
    with open(log_file, 'a') as f:
        f.write('STEP 1 ended'+'\n')
        f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
    return steps_status[1]
