import os
import yaml
from time import sleep
import shutil
from fireworks.core.fworker import FWorker
from fireworks.core.launchpad import LaunchPad
from fireworks.queue.queue_launcher import rapidfire
from fireworks.utilities.fw_serializers import load_object_from_file
from workflows.config import * 

launchpad = LaunchPad.from_file(os.path.join(config_dir,'my_launchpad.yaml'))

def launchpad_reset():
    try:
        launchpad.reset('', require_password = False, max_reset_wo_password = 10000)
        return True
    except:
        return False

def add_wf(wf):
    launchpad.add_wf(wf)

def run_jobs(job_type, l_dir):
    qadapter = load_object_from_file(os.path.join(config_dir,'my_qadapter.yaml'))
    fworker = FWorker.from_file(os.path.join(config_dir,'my_fworker.yaml'))

    job_script = config_list['job_script']

    qadapter['job_name'] = config_list['JOB_NAME'] if config_list['JOB_NAME'] else 'pyflame'

    if job_script['default']['partition']:
        qadapter['queue'] = job_script['default']['partition']
    if job_script['default']['account']:
        qadapter['account'] = job_script['default']['account']
    if job_script['default']['time']:
        qadapter['time'] = str(job_script['default']['time'])
    if job_script['default']['ntasks-per-node']:
        qadapter['ntasks-per-node'] = job_script['default']['ntasks-per-node']

    with open(os.path.join(PyFLAME_directory,'workflows/job_script/list_of_jobtypes.yaml'), 'r') as f:
        ljts = yaml.load(f, Loader=yaml.FullLoader)
    job_type_list = ljts['job_type_list']

    if job_type in job_type_list:
        if job_script[job_type]['number_of_jobs']:
            nqueue = job_script[job_type]['number_of_jobs']
        else:
            return False
        if job_script[job_type]['nodes']:
            qadapter['nodes'] = job_script[job_type]['nodes']
        if job_script[job_type]['ntasks']:
            qadapter['ntasks'] = job_script[job_type]['ntasks']
        if job_script[job_type]['partition']:
            qadapter['queue'] = job_script[job_type]['partition']
        if job_script[job_type]['time']:
            qadapter['time'] = str(job_script[job_type]['time'])
        if job_script[job_type]['launching_mode']:
            if job_script[job_type]['launching_mode'] == 'multiple':
                if job_script[job_type]['number_of_workers']:
                    qadapter['rocket_launch'] = ' rlaunch -c ' +  config_dir + ' multi ' +  str(job_script[job_type]['number_of_workers'])
                else:
                    return False
            elif job_script[job_type]['launching_mode'] == 'rapidfire':
                qadapter['rocket_launch'] = ' rlaunch -c ' +  config_dir + ' rapidfire'
            elif job_script[job_type]['launching_mode'] == 'singleshot':
                qadapter['rocket_launch'] = ' rlaunch -c ' +  config_dir + ' singleshot'
            else:
                return False

        if job_type == 'geopt' or job_type == 'nsw0':
            if input_list['ab_initio_code'] == 'VASP':
                if config_list['VASP_template']:
                    qadapter.template_file = os.path.join(config_dir, config_list['VASP_template'])
        else:
            if config_list['FLAME_template']:
                qadapter.template_file = os.path.join(config_dir, config_list['FLAME_template'])
    else:
        return False

    try:
        rapidfire(launchpad, fworker, qadapter, launch_dir=l_dir, njobs_queue=nqueue, nlaunches=0, reserve=False)
        return True
    except:
        return False

def run_exists():
    state = False
    if launchpad.get_fw_ids(query={'state': 'RUNNING'}):
        state = True
    return state

def check_lp(wfname_list):
    for wfname in wfname_list:
        try:
            wfid = launchpad.get_wf_ids({"name": wfname})
            wf_summary = launchpad.get_wf_summary_dict(wfid[0],mode="less")
        except:
            return 'unknown'
    wf_state_list = []
    for wfname in wfname_list:
        wfid = launchpad.get_wf_ids({"name": wfname})
        wf_state = launchpad.get_wf_summary_dict(wfid[0],mode="less")['state']
        wf_state_list.append(wf_state)

    if 'FIZZLED' in wf_state_list:
        return 'FIZZLED'
    else:
        return True

def fizzle_lostruns(ftime):
    launchpad.detect_lostruns(expiration_secs = ftime, fizzle = True, refresh = True)

def fizzle_lostruns_task(ftime):
    while True:
        launchpad.detect_lostruns(expiration_secs = ftime, fizzle = True, refresh = True)
        if len(launchpad.get_fw_ids(query={'state': 'RUNNING'})) > 1:
            sleep(300)
        else:
            break
    fw_ids = []
    if launchpad.future_run_exists():
        fwids = launchpad.get_fw_ids(query={'state': 'WAITING'})
        fw_ids.append(fwids)
        fwids = launchpad.get_fw_ids(query={'state': 'READY'})
        fw_ids.append(fwids)
    launchpad.delete_fws(fw_ids)

def rerun():
    fwids = []
    fwids = launchpad.get_fw_ids(query={"state":"READY"})
    if fwids: 
        wfid =  launchpad.get_wf_by_fw_id(fwids[0])
        if wfid:
            wf_name = wfid.name
            if 'step-2' in wf_name:
               run_jobs('geopt',output_dir)
            elif 'FLAME_SP' in wf_name:
                run_jobs('flame_sp', output_dir)
            elif 'VASP_NSW0' in wf_name:
                run_jobs('nsw0', output_dir)
            elif 'flame' in wf_name:
                stpnm = wf_name.split('_')[1]
                run_jobs(stpnm, output_dir)
            else:
                return False
    else:
        return False

def find_block_folder():
    block_dir_list = []
    for root, dirs, files in os.walk(output_dir):
        for a_dir in dirs:
           if 'block' in a_dir:
               block_dir_list.append(os.path.join(root,a_dir))
    if len(block_dir_list) > 0:
        return(block_dir_list)
    else:
        return(None)

def clean_after_run():
    block_dir_list = find_block_folder()
    if block_dir_list:
        block_dir = block_dir_list[0]
        if input_list['keep_all_files']:
            try:
                os.mkdir(debug_dir)
            except FileExistsError:
                pass
            for file_name in os.listdir(block_dir):
                shutil.move(os.path.join(block_dir,file_name), os.path.join(debug_dir,file_name))
        else:
            for file_name in os.listdir(block_dir):
                shutil.rmtree(os.path.join(block_dir,file_name))



def extend_t():
    with open('task.json','r') as f:
        task_f =  yaml.load(f, Loader=yaml.FullLoader)
    e_time = task_f['run_stats']['overall']['Elapsed time (sec)']
    if e_time < 61:
        sleep(61-e_time)
