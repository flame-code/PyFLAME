import os
import sys
import json
import yaml
import shutil
from datetime import datetime
from collections import defaultdict
from pymatgen.core.structure import Structure, Molecule
from pymatgen.analysis.structure_matcher import StructureMatcher, ElementComparator
from workflows.core import *
from workflows.config import *

def step_3():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 3'+'\n')
        f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

    block_dir_list = find_block_folder()
    if block_dir_list:
        if len(block_dir_list) > 1:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
            exit()
        block_dir = block_dir_list[0]
    else:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed: calculation directory was not found <<<'+'\n')
        exit()

    try:
        os.mkdir(Flame_dir)
    except:
        pass
    try:
        os.mkdir(os.path.join(Flame_dir,'step-0'))
    except:
        with open(log_file, 'a') as f:
            f.write('>>> Cannot procceed: {} exists <<<'.format(os.path.join(Flame_dir,'step-0'))+'\n')
        exit()
    os.mkdir(os.path.join(Flame_dir,'step-0','task_files'))
    os.mkdir(os.path.join(Flame_dir,'step-0','excluded_task_files'))

    relaxed_bulk_structs_list = []
    nonduplicated_structs_list = [] 
    nonduplicated_structs_epot = [] 
    nonduplicated_structs_epa  = []  
    seen_structs = []               
    seenstruct = {}

    cluster_structures = []
    minhopp_seeds = []
    minhocao_seeds = []
    sm = StructureMatcher(comparator=ElementComparator(),primitive_cell=True)

    for root, dirs, files in os.walk(block_dir):
        if 'task.json' in files:
            try:
                with open(os.path.join(root,'task.json'), 'r') as f:
                    s = json.loads(f.read())
            except:
                with open(log_file, 'a') as f:
                    f.write('cannt open task file in {}'.format(root)+'\n')
                continue
            # remove duplicate bulk structures
            if s["task_label"] == 'bulk' and s['output']['energy'] < 0:
                relaxed_bulk_structs_list.append(s['output']['structure'])
                found = False
                for seenstruct in seen_structs:
                    if sm.fit(Structure.from_dict(s['output']['structure']), Structure.from_dict(seenstruct)):
                        found = True
                        break
                if not found:
                    nonduplicated_structs_list.append(s['output']['structure'])
                    nonduplicated_structs_epot.append(s['output']['energy'])
                    nonduplicated_structs_epa.append(s['output']['energy_per_atom'])
                    seen_structs.append(s['output']['structure'])

    min_epa = min(nonduplicated_structs_epa)
    max_epa = max(nonduplicated_structs_epa)
    ave_epa = sum(nonduplicated_structs_epa)/len(nonduplicated_structs_epa)

    # store minimum energy/atom
    with open(os.path.join(random_structures_dir,'epa.dat'), 'w') as f:
        f.write('min_epa: {}'.format(min_epa)+'\n')
        f.write('max_epa: {}'.format(max_epa)+'\n')
        f.write('ave_epa: {}'.format(ave_epa)+'\n')

    # store average energy/atom
    with open(os.path.join(random_structures_dir,'min_epa.dat'), 'w') as f:
        f.write(str(min_epa))

    # remove too high and too low energy bulk structures
    forplot = []
    final_bulk_structure_list = []
    for i in range(len(nonduplicated_structs_list)):
        if nonduplicated_structs_epa[i] <= min_epa + 1.5:
            forplot.append([len(Structure.from_dict(nonduplicated_structs_list[i]).sites), nonduplicated_structs_epa[i]])
            final_bulk_structure_list.append(nonduplicated_structs_list[i])

    # cp task files
    for root, dirs, files in os.walk(block_dir):
        if 'task.json' in files:
            try:
                with open(os.path.join(root,'task.json'), 'r') as f:
                    s = json.loads(f.read())
            except:
                continue

            if s["task_label"] == 'opt2' or s["task_label"] == 'bulk' or s["task_label"] == 'scaled_bulk':
                epa = s['output']['energy_per_atom']
                if epa <= min_epa + 1.5:
                    new_task_name = "task" + str(epa) + '.json'
                    shutil.copyfile(os.path.join(root,'task.json'), os.path.join(Flame_dir,'step-0','task_files',new_task_name))
                    # seeds for minhopp and minhocao 
                    if s["task_label"] == 'opt2' or s["task_label"] == 'bulk':
                        minhocao_seeds.append(s['output']['structure'])
                        structure_from_task = Structure.from_dict(s['output']['structure'])
                        spcs = []
                        crdnts = structure_from_task.cart_coords
                        for coord in structure_from_task.sites:
                            spcs.append(coord.species_string)
                        molecule = Molecule(spcs, crdnts)
                        try:
                            boxed_cluster = molecule.get_boxed_structure(input_list['box_size'],input_list['box_size'],input_list['box_size'])
                            cluster_structures.append(boxed_cluster.as_dict())
                        except ValueError:
                            pass
                    if s["task_label"] == 'scaled_bulk' or s["task_label"] == 'bulk':
                        minhopp_seeds.append(s['output']['structure'])
                else:
                    new_task_name = "excluded" + str(epa) + '.json'
                    shutil.copyfile(os.path.join(root,'task.json'), os.path.join(Flame_dir,'step-0','excluded_task_files',new_task_name))

    # store nat_epa for bulk structures 
    with open(os.path.join(random_structures_dir,'nat_epa.dat'), 'w') as f:
        for i in range(len(forplot)):
            f.write('{} {}'.format(str(forplot[i][0]), str(forplot[i][1]))+'\n')
    # store bulk structures
    with open(os.path.join(random_structures_dir,'optimized_random_bulk_structures.json'), 'w') as f:
        json.dump(final_bulk_structure_list, f)
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write("number of finished jobs for bulk geometry optimization: {}".format(len(relaxed_bulk_structs_list))+'\n')
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write("Number of relaxed bulk structures after removing duplicated and high energy structures: {}".format(len(final_bulk_structure_list))+'\n')
    # store random cluster structures
    with open(os.path.join(random_structures_dir,'random_cluster_structures.json'),'w') as f:
        json.dump(cluster_structures, f)
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write('{} cluster structures are generated.'.format(len(cluster_structures))+'\n')
    # store stressed structures 
    with open(os.path.join(random_structures_dir,'minhopp_seeds.json'),'w') as f:
        json.dump(minhopp_seeds, f)
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write('{} minhopp seed structures are stored.'.format(len(minhopp_seeds))+'\n')
    with open(os.path.join(random_structures_dir,'minhocao_seeds.json'),'w') as f:
        json.dump(minhocao_seeds, f)
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write('{} minhocao seed structures are stored.'.format(len(minhocao_seeds))+'\n')
    
    # mv/rm calculation files
    clean_after_run()

    with open(log_file, 'a') as f:
        f.write('STEP 3 ended'+'\n')
        f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
    return steps_status[3] 
