import os
import json
from time import sleep
from datetime import datetime
from sys import exit
from random import sample
from collections import defaultdict
from fireworks import Firework, PyTask
from pymatgen.core.structure import Structure
from structure.core import *
from workflows.core import *
from workflows.config import *

def step_2():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 2'+'\n')
        f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
    # check the lpad LPAD
    if run_exists():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
        exit()

    block_dir_list = find_block_folder()
    if block_dir_list:
        if len(block_dir_list) > 1:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
            exit()
        block_dir = block_dir_list[0]
        if len(os.listdir(block_dir)) != 0:
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
            exit()

    # get compositions and elements 
    composition_list = read_composition_list() 
    if not composition_list:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: no composition is provided <<<'+'\n')
        exit()

    allowed_n_atom = get_allowed_n_atom_for_compositions(composition_list)

    allowed_n_atom_reference = []
    for r_n_a in input_list['reference_number_of_atoms']:
        if r_n_a in allowed_n_atom:
            allowed_n_atom_reference.append(r_n_a)

    n_struct_geopt = int(input_list['max_number_of_bulk_structures']/(len(allowed_n_atom)+len(allowed_n_atom_reference)))
    # read known structures if any
    known_bulk_structures_dict = defaultdict(list)
    if os.path.exists(os.path.join(random_structures_dir,'known_bulk_structures.json')):
        with open(log_file, 'a') as f:
            f.write('Reading known bulk structures'+'\n')
        with open(os.path.join(random_structures_dir,'known_bulk_structures.json'), 'r') as f:
            k_b_s = json.loads(f.read())
        for a_k_b_s in k_b_s:
            known_bulk_structures_dict[len(Structure.from_dict(a_k_b_s).sites)].append(Structure.from_dict(a_k_b_s))

    all_bulk_structures_dict = defaultdict(list)

    if input_list['read_structure_from_local_db']:
        with open(log_file, 'a') as f: 
             f.write('Reading bulk structures from the local database'+'\n')
        db_bulk_structures_dict = read_bulk_structure_from_db(composition_list, input_list['format_of_structures'], input_list['anonymous_formula'])
        for a_n_a in allowed_n_atom:
            if a_n_a in db_bulk_structures_dict.keys():
                n_struct = n_struct_geopt
                if a_n_a in allowed_n_atom_reference and n_struct - len(known_bulk_structures_dict[a_n_a]) > 0:
                    n_struct = 2 * n_struct - len(known_bulk_structures_dict[a_n_a])
                if input_list['random_structure_generation'] != 0:
                    n_struct = int(n_struct/2)
                if len(db_bulk_structures_dict[a_n_a]) > 0:
                    all_bulk_structures_dict[a_n_a].extend(sample(db_bulk_structures_dict[a_n_a], n_struct))

    if input_list['random_structure_generation'] != 0:
        with open(log_file, 'a') as f:
             f.write('Reading random bulk structures'+'\n')
        if os.path.exists(os.path.join(random_structures_dir,'random_bulk_structures.json')):
            with open(os.path.join(random_structures_dir,'random_bulk_structures.json'), 'r') as f:
                random_bulk_structures_dict = json.loads(f.read())
            for a_n_a in allowed_n_atom:
                if str(a_n_a) in random_bulk_structures_dict.keys():
                    n_struct = n_struct_geopt
                    if a_n_a in allowed_n_atom_reference and n_struct > len(known_bulk_structures_dict[a_n_a]):
                        n_struct = 2 * n_struct - len(known_bulk_structures_dict[a_n_a])
                    if len(random_bulk_structures_dict[str(a_n_a)]) >= n_struct-len(all_bulk_structures_dict[a_n_a]):
                        all_bulk_structures_dict[a_n_a].extend(sample(random_bulk_structures_dict[str(a_n_a)], n_struct-len(all_bulk_structures_dict[a_n_a])))
        else: 
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: no random bulk structure was found <<<'+'\n')
            exit()
    structure_opt2_geopt = []
    structure_bulk_geopt = []

    for a_n_a in allowed_n_atom:
        if len(all_bulk_structures_dict[a_n_a]) == 0:
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: no atomic structure for structures with {} atoms for optimization <<<'.format(a_n_a)+'\n')
        else:
            indices = list(range(0,len(all_bulk_structures_dict[a_n_a])))
            indices_opt2 = sample(indices, n_struct_geopt)
            for rem in indices_opt2:
                indices.remove(rem)

            indices_bulk = indices
            for i in indices_opt2:
                structure_opt2_geopt.append(Structure.from_dict(all_bulk_structures_dict[a_n_a][i]))
            if a_n_a in input_list['reference_number_of_atoms']:
                for i in indices_bulk:
                    structure_bulk_geopt.append(Structure.from_dict(all_bulk_structures_dict[a_n_a][i]))
                if len(known_bulk_structures_dict[a_n_a]) < n_struct_geopt:
                    structure_bulk_geopt.extend(known_bulk_structures_dict[a_n_a])
                else:
                    structure_bulk_geopt.extend(sample(known_bulk_structures_dict[a_n_a], n_struct_geopt))

    fizzle_lostruns_task = PyTask(func='workflows.core.fizzle_lostruns_task', args = [3600])
    firework = Firework(fizzle_lostruns_task, name = 'job_control', spec={'_priority': 1})
    add_wf(firework)

    # VASP
    if input_list['ab_initio_code'] == 'VASP':
        from VASP.core import get_multistep_bulk_optimization_wf, get_opt2_wf
        with open(log_file, 'a') as f:
            f.write('Ab-initio calculations with VASP'+'\n')
        step2_bulk_wf = get_multistep_bulk_optimization_wf(structure_bulk_geopt, wf_name = 'step-2')
        step2_opt1_wf = get_opt2_wf(structure_opt2_geopt, wf_name = 'step-2')
        # add bulk workflow
        add_wf(step2_bulk_wf)
        add_wf(step2_opt1_wf)
    # CP2K
    elif input_list['ab_initio_code'] == 'CP2K':
        with open(log_file, 'a') as f:
            f.write('Ab-initio calculations with CP2K'+'\n')
        exit()
    # QE
    elif input_list['ab_initio_code'] == 'QE':
        with open(log_file, 'a') as f:
            f.write('Ab-initio calculations with Quantum Espresso'+'\n')
        exit()
    else:
        with open(log_file, 'a') as f:
            f.write('>>> No ab-initio code is provided <<<'+'\n')
        exit()

    # run jobs
    if not run_jobs('geopt',output_dir):
        with open(log_file, 'a') as f:
            f.write('>>> Cannot run jobs for VASP geometry optimization <<<'+'\n')
        exit()
    # wait until all jobs are done
    while True:
        fizzle_lostruns(3600)
        if run_exists():
            sleep(60)
        else:
           break
    # check launchpad state
    lp_state = check_lp(['step-2']) 
    if lp_state == 'FIZZLED':
        with open(log_file, 'a') as f:
            f.write('>>> WARNING: step-2 geometry optimization workflow is FIZZLED <<<'+'\n')
    if lp_state == 'unknown':
        with open(log_file, 'a') as f:
            f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
        exit()

    with open(log_file, 'a') as f:
        f.write('STEP 2 ended'+'\n')
        f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
    return steps_status[2] 

