import os 
import yaml
import json
import pymongo
from fireworks.fw_config import CONFIG_FILE_DIR 

def config_set(run_dir):
    with open(os.path.join(CONFIG_FILE_DIR,'db.json'), 'r') as f:
        d = json.loads(f.read())

    myclient = pymongo.MongoClient(d['host'], username =  d['admin_user'], password = d['admin_password'])
    mydb = myclient["path"]
    mycol = mydb["directories"]
    mycol.delete_many({})
    dir_dict = {"run_dir": run_dir}
    mycol.insert_one(dir_dict)
