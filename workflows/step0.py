import os
from sys import exit
from datetime import datetime
from structure.core import *
from workflows.core import *
from workflows.config import *

def step_0():
    try:
        os.mkdir(output_dir)
    except FileExistsError:
        pass

    with open(log_file, 'a') as f:
        f.write('Starting PyFLAME'+'\n')
        f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

    try:
        os.mkdir(random_structures_dir)
    except FileExistsError:
        with open(log_file, 'a') as f:
            f.write(">>> Cannot proceed: {} exists <<<".format(random_structures_dir)+'\n')
        exit()
    # get compositions and elements 
    composition_list, element_list = get_compositions_elements()
    if not composition_list:
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: no composition is provided <<<'+'\n')
        exit()
    # get_known_structures
    get_known_structures(composition_list)
    # write compositions and elements
    write_elements(element_list)
    write_compositions(composition_list)
    # reset LPAD
    if run_exists():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
        exit()
    elif not launchpad_reset():
        with open(log_file, 'a') as f:
            f.write('>>> ERROR: cannot reset Launchpad <<<'+'\n')
        exit()
    else:
        with open(log_file, 'a') as f:
            f.write('Launchpad was reset'+'\n')
    return steps_status[0]
