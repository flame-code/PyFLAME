import os
import sys
import shutil
import json
import yaml
import math
import numpy as np
from sys import exit
from datetime import datetime
from collections import defaultdict
from pymatgen.core.structure import Structure, Molecule
from workflows.core import *
from flame.workflows import *
from flame.aver_dist import compute_aver_dist
from flame.train import select_a_train, collect_training_data
from structure.core import *
from workflows.core import find_block_folder
from workflows.config import *
 
def step_4():
    with open(log_file, 'a') as f:
        f.write("---------------------------------------------------------------------------------------------------"+'\n')
        f.write('STEP 4'+'\n')
    # calculate the aver_dist for pickdiff if it is not already calculated
    if not os.path.exists(os.path.join(Flame_dir,'aver_dist','aver_dist.json')):
        with open(log_file, 'a') as f:
            f.write("-----------------------------------------------"+'\n')
            f.write('Aaverage distance calculation'+'\n')
            f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

        if run_exists():
            with open(log_file, 'a') as f:
                f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
            exit()

        block_dir_list = find_block_folder()
        if block_dir_list:
            if len(block_dir_list) > 1:
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                exit()
            block_dir = block_dir_list[0]
            if len(os.listdir(block_dir)) != 0:
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
                exit()

        # creating directories
        try:
            os.mkdir(os.path.join(Flame_dir,'aver_dist'))
        except FileExistsError:
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,'aver_dist'))+'\n')
            exit()

        # get average distance wf
        aver_dist_wf = get_aver_dist_wf()
        # add workflow
        add_wf(aver_dist_wf)
        # run average distance jobs
        if not run_jobs('aver_dist', output_dir):
            with open(log_file, 'a') as f:
                f.write('>>> Cannot run jobs for average distance calculations <<<'+'\n')
            exit()
        # wait until all jobs are done
        while True:
            fizzle_lostruns(3600)
            if run_exists():
                sleep(60)
            else:
                break
        # check launchpad status
        lp_state = check_lp(['aver_dist'])
        if lp_state == 'FIZZLED':
            with open(log_file, 'a') as f:
                f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
        elif lp_state == 'unknown':
            with open(log_file, 'a') as f:
                f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
            exit()

        # calculae average distances and store it
        compute_aver_dist()
        # mv/rm calculation files
        clean_after_run()
       
        with open(log_file, 'a') as f:
            f.write('Aaverage distance calculation ended'+'\n')
            f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
####################FLAME LOOP####################
    FLAME_step_name = restart['FLAME_loop_start'][1]
    for c_s_n in range(restart['FLAME_loop_start'][0], restart['FLAME_loop_stop'][0]+1):
        step_number = 'step-'+str(c_s_n)
######FLAME train######
        if FLAME_step_name == 'train':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: train calculations'.format(step_number)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            if len(input_list['number_of_nodes']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                exit()

            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()

            block_dir_list = find_block_folder()
            if block_dir_list:
                if len(block_dir_list) > 1:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                    exit()
                block_dir = block_dir_list[0]
                if len(os.listdir(block_dir)) != 0:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
                    exit()

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number))
            except FileExistsError:
                pass
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'train'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'train'))+'\n')
                exit()

            # first collect and store data from the previous step
            collect_training_data(step_number)

            # get train wf
            flame_train_wf = get_train_wf(step_number)
            # add workflow
            add_wf(flame_train_wf)
            # run train jobs
            if not run_jobs('train', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run jobs for training <<<'+'\n')
                exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(14400)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            lp_state = check_lp(['flame_train_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
                exit()
            # select the best trainig among the 3
            select_a_train(step_number)
            # mv/rm calculation files
            clean_after_run()

            with open(log_file, 'a') as f:
                f.write('FLAME-{}: train calculations ended'.format(step_number)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'train':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                exit()
            else:
                FLAME_step_name = 'minhocao'
######FLAME minhocao######
        if FLAME_step_name == 'minhocao':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: minhocao calculations'.format(step_number)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            if len(input_list['minhocao_steps']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                exit()

            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()

            block_dir_list = find_block_folder()
            if block_dir_list:
                if len(block_dir_list) > 1:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                    exit()
                block_dir = block_dir_list[0]
                if len(os.listdir(block_dir)) != 0:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
                    exit()

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'minhocao'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'minhocao'))+'\n')
                exit()
            # get minhocao wf
            flame_minhocao_wf = get_minhocao_wf(step_number)
            # add workflow
            add_wf(flame_minhocao_wf)
            # run minhocao jobs
            if not run_jobs('minhocao', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run jobs for minhocao <<<'+'\n')
                exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(3600)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            lp_state = check_lp(['flame_minhocao_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
                exit()

            with open(log_file, 'a') as f:
                f.write('FLAME-{}: minhocao calculations ended'.format(step_number)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'minhocao':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                exit()
            else:
                FLAME_step_name = 'minhocao_store'
######FLAME minhocao_store######
        if FLAME_step_name == 'minhocao_store':

            if len(input_list['minhocao_steps']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                exit()

            block_dir_list = find_block_folder()
            if block_dir_list:
                if len(block_dir_list) > 1:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                    exit()
                block_dir = block_dir_list[0]
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: calculation directory was not found <<<'+'\n')
                exit()

            # get minhocao_store wf
            minhocao_store_wf = get_minhocao_store_wf(step_number)
            # add workflow
            add_wf(minhocao_store_wf)
            # run minhocao jobs
            if not run_jobs('minhocao_store', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot store minhocao results <<<'+'\n')
                exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(3600)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            lp_state = check_lp(['minhocao_store_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannor proceed: minhocao store is fizzled <<<'+'\n')
                exit()
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
                exit()
            # mv/rm calculation files
            clean_after_run()

            with open(log_file, 'a') as f:
                f.write('FLAME-{}: minhocao results are stored'.format(step_number)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'minhocao_store':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                exit()
            else:
                FLAME_step_name = 'minhopp'
######FLAME minhopp######
        if FLAME_step_name == 'minhopp':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: minhopp calculations'.format(step_number)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            if len(input_list['minhopp_steps']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                exit()

            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()

            block_dir_list = find_block_folder()
            if block_dir_list:
                if len(block_dir_list) > 1:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                    exit()
                block_dir = block_dir_list[0]
                if len(os.listdir(block_dir)) != 0:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
                    exit()

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'minhopp'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'minhopp'))+'\n')
                exit()
            # get minhopp wf
            flame_minhoppc_wf, flame_minhoppb_wf = get_minhopp_wf(step_number)
            # add workflow
            if input_list['cluster_calculation']:
                if flame_minhoppc_wf:
                    add_wf(flame_minhoppc_wf)
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: no minhopp calculations for clusters <<<'+'\n')
            if flame_minhoppb_wf:
                add_wf(flame_minhoppb_wf)
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: no minhopp calculations for bulk structures <<<'+'\n')

            # run minhopp jobs
            if not run_jobs('minhopp', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run jobs for minhopp <<<'+'\n')
                exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(3600)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            lp_state = check_lp(['flame_minhopp_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
                exit()

            with open(log_file, 'a') as f:
                f.write('FLAME-{}: minhopp calculations ended'.format(step_number)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'minhopp':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                exit()
            else:
                FLAME_step_name = 'minhopp_store'
######FLAME minhopp_store######
        if FLAME_step_name == 'minhopp_store':

            if len(input_list['minhopp_steps']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                exit()

            block_dir_list = find_block_folder()
            if block_dir_list:
                if len(block_dir_list) > 1:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                    exit()
                block_dir = block_dir_list[0]
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: calculation directory was not found <<<'+'\n')
                exit()

            # get minhopp_store wf
            minhopp_store_wf = get_minhopp_store_wf(step_number)
            # add workflow
            add_wf(minhopp_store_wf)
            # run minhopp jobs
            if not run_jobs('minhopp_store', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run minhopp store job <<<'+'\n')
                exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(3600)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            lp_state = check_lp(['minhopp_store_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: minhopp store is fizzled  <<<'+'\n')
                exit()
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: the state of Workflow is not known.<<<'+'\n')
                exit()
            # mv/rm calculation files
            clean_after_run()

            with open(log_file, 'a') as f:
                f.write('FLAME-{}: minhopp results are stored'.format(step_number)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'minhopp_store':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                exit()
            else:
                FLAME_step_name = 'divcheck'
######FLAME divcheck######
        if FLAME_step_name == 'divcheck':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: divcheck calculations'.format(step_number)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            if len(input_list['dtol_prefactor']) < c_s_n:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: parameters for step-{} {} are not provided <<<'.format(c_s_n, FLAME_step_name)+'\n')
                exit()

            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()

            block_dir_list = find_block_folder()
            if block_dir_list:
                if len(block_dir_list) > 1:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                    exit()
                block_dir = block_dir_list[0]
                if len(os.listdir(block_dir)) != 0:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
                    exit()

            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'divcheck'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'divcheck'))+'\n')
                exit()
            # get divcheck workflow
            flame_divcheck_b_wf = get_divcheck_b_wf(step_number)
            # add workflow
            if flame_divcheck_b_wf:
                add_wf(flame_divcheck_b_wf)
            else:
                with open(log_file, 'a') as f:
                     f.write('>>> WARNING: no divcheck for bulk structures <<<'+'\n')
            # for cluster
            if input_list['cluster_calculation']:
                # get divcheck workflow
                flame_divcheck_c_wf = get_divcheck_c_wf(step_number)
                # add workflow
                if flame_divcheck_c_wf:
                    add_wf(flame_divcheck_c_wf)
                else:
                     with open(log_file, 'a') as f:
                         f.write('>>> WARNING: no divcheck for clusters <<<'+'\n')
            # run divcheck jobs
            if not run_jobs('divcheck', output_dir):
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot run divcheck jobs <<<'+'\n')
                exit()
            # wait until all jobs are done
            while True:
                fizzle_lostruns(3600)
                if run_exists():
                    sleep(60)
                else:
                    break
            # check launchpad status
            lp_state = check_lp(['flame_divcheck_'+str(step_number)])
            if lp_state == 'FIZZLED':
                with open(log_file, 'a') as f:
                    f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
            elif lp_state == 'unknown':
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
                exit()
            # mv/rm calculation files
            clean_after_run()

            with open(log_file, 'a') as f:
                f.write('FLAME-{}: divcheck calculations ended'.format(step_number)+'\n')
                f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'divcheck':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                exit()
            else:
                FLAME_step_name = 'SP_calculations'
###single point calculations 
        if FLAME_step_name == 'SP_calculations':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{}: single point calculations'.format(step_number)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            if run_exists():
                with open(log_file, 'a') as f:
                    f.write('>>> ERROR: unfinished jobs in Launchpad <<<'+'\n')
                exit()

            block_dir_list = find_block_folder()
            if block_dir_list:
                if len(block_dir_list) > 1:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                    exit()
                block_dir = block_dir_list[0]
                if len(os.listdir(block_dir)) != 0:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: {} is not empty <<<'.format(block_dir)+'\n')
                    exit()

            SPF_bulk_structures = defaultdict(list)
            SPF_clusters = defaultdict(list)
            # add bulk (and cluster) structures from divcheck
            for a_file in os.listdir(os.path.join(Flame_dir,step_number,'divcheck')):
                nat = a_file.split('_')[0].split('-')[0]
                with open(os.path.join(Flame_dir,step_number,'divcheck',a_file) ,'r') as f:
                    confs = json.loads(f.read())
                for conf in confs:
                    lattice = conf['conf']['cell']
                    crdnts = []
                    spcs = []
                    for coord in conf['conf']['coord']:
                        crdnts.append([coord[0],coord[1],coord[2]])
                        spcs.append(coord[3])
                    if 'bulk' in a_file:
                        struct = Structure(lattice, spcs, crdnts, coords_are_cartesian = True)
                        SPF_bulk_structures[nat].append(struct)
                    if 'cluster' in a_file and input_list['cluster_calculation']:
                        array_crdnts = np.array(crdnts)
                        maxx = max(array_crdnts[:,0:1])[0]
                        minx = min(array_crdnts[:,0:1])[0]
                        maxy = max(array_crdnts[:,1:2])[0]
                        miny = min(array_crdnts[:,1:2])[0]
                        maxz = max(array_crdnts[:,2:3])[0]
                        minz = min(array_crdnts[:,2:3])[0]
                        a_cluster = maxx-minx+5
                        b_cluster = maxy-miny+5
                        c_cluster = maxz-minz+5
                        if a_cluster > input_list['box_size'] or b_cluster > input_list['box_size'] or c_cluster > input_list['box_size']:
                            continue 
                        molecule = Molecule(spcs, crdnts)
                        try:
                            boxed_molecule = molecule.get_boxed_structure(a_cluster,b_cluster,c_cluster)
                        except:
                            continue
                        SPF_clusters[nat].append(boxed_molecule)
            # FLAME SP? 
            if input_list['FLAME_SP'][c_s_n-1]:
                # FLAME singlepoint calculations
                with open(log_file, 'a') as f:
                    f.write("FLAME single point calculations"+'\n')
                wfname_list = []
                for keys in SPF_bulk_structures:
                    if len(SPF_bulk_structures[keys]) > 0:
                        wfn = 'FLAME_SP_'+str(keys)+'-atom_bulk_'+step_number
                        flame_sp_bulk_wf = get_flame_sp_wf(step_number, SPF_bulk_structures[keys], 'bulk', wfn)
                        add_wf(flame_sp_bulk_wf)
                        wfname_list.append(wfn)
                if input_list['cluster_calculation']:
                    for keys in SPF_clusters:
                        if len(SPF_clusters[keys]) > 0:
                            wfn = 'FLAME_SP_'+str(keys)+'-atom_cluster_'+step_number
                            flame_sp_cluster_wf = get_flame_sp_wf(step_number, SPF_clusters[keys], 'cluster', wfn)
                            add_wf(flame_sp_cluster_wf)
                            wfname_list.append(wfn)
                # run FLAME jobs
                if not run_jobs('flame_sp', output_dir):
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot run FLAME single point calculations <<<'+'\n')
                    exit()
                # wait until all jobs are done
                while True:
                    fizzle_lostruns(3600)
                    if run_exists():
                        sleep(60)
                    else:
                        break
                # check launchpad status
                lp_state = check_lp(wfname_list)
                if lp_state == 'FIZZLED':
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
                elif lp_state == 'unknown':
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
                    exit()

                # decide if a sturcture should be send for ab_initio single point calculations
                SP_bulk_structures = defaultdict(list)
                SP_clusters = defaultdict(list)

                # collect FLAME SP calculation results
                block_dir_list = find_block_folder()
                if block_dir_list:
                    if len(block_dir_list) > 1:
                        with open(log_file, 'a') as f:
                            f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                        exit()
                    block_dir = block_dir_list[0]
                else:
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot proceed: calculation directory was not found <<<'+'\n')
                    exit()

                for root, dirs, files in os.walk(block_dir):
                    if 'posinp.vasp' in files:
                        struct = Structure.from_file(os.path.join(root,'posinp.vasp'))
                        nat = len(struct.sites)
                        if os.path.exists(os.path.join(root,'energies.dat')):
                            with open(os.path.join(root,'energies.dat'), 'r') as f:
                                energies = [float(line.strip()) for line in f]
                            if len(energies) < 3:
                                with open(os.path.join(root,'FW.json'), 'r') as f:
                                    fw_file = json.loads(f.read())
                                job_type = fw_file["name"]
                                if job_type == 'bulk':
                                    SP_bulk_structures[nat].append(struct)
                                elif job_type == 'cluster':
                                    SP_clusters[nat].append(struct)
                            else:
                                d_e = abs(max(energies)- min(energies))/nat
                                if d_e > 0.000735:
                                    with open(os.path.join(root,'FW.json'), 'r') as f:
                                        fw_file = json.loads(f.read())
                                    job_type = fw_file["name"]
                                    if job_type == 'bulk':
                                        SP_bulk_structures[nat].append(struct)
                                    elif job_type == 'cluster':
                                        SP_clusters[nat].append(struct)
                        else:
                            with open(os.path.join(root,'FW.json'), 'r') as f:
                                fw_file = json.loads(f.read())
                            job_type = fw_file["name"]
                            if job_type == 'bulk':
                                SP_bulk_structures[nat].append(struct)
                            elif job_type == 'cluster':
                                SP_clusters[nat].append(struct)

                # mv/rm calculation files
                clean_after_run()
            else:
                SP_bulk_structures = SPF_bulk_structures
                SP_clusters =  SPF_clusters
            # VASP
            if input_list['ab_initio_code'] == 'VASP':
                from VASP.core import get_nsw0_wf
                with open(log_file, 'a') as f:
                    f.write("VASP single point calculations"+'\n')

                wfname_list = []
                for keys in SP_bulk_structures:
                    if len(SP_bulk_structures[keys]) > 0:
                        wfn = 'VASP_NSW0_'+str(keys)+'-atom_bulk_'+step_number
                        vasp_nsw0_bulk_wf = get_nsw0_wf(SP_bulk_structures[keys], wfn, 'bulk')
                        add_wf(vasp_nsw0_bulk_wf) 
                        wfname_list.append(wfn)
                    else:
                        with open(log_file, 'a') as f:
                            f.write('>>> WARNING: no atomic configuration for {}-atom bulk structures <<<'.format(str(keys))+'\n')

                if input_list['cluster_calculation']:
                    for keys in SP_clusters:
                        if len(SP_clusters[keys]) > 0:
                            wfn = 'VASP_NSW0_'+str(keys)+'-atom_cluster_'+step_number
                            vasp_nsw0_cluster_wf = get_nsw0_wf(SP_clusters[keys], wfn, 'cluster')
                            add_wf(vasp_nsw0_cluster_wf)
                            wfname_list.append(wfn)
                        else:
                            with open(log_file, 'a') as f:
                                f.write('>>> WARNING: no atomic configuration for {}-atom clusters <<<'.format(str(keys))+'\n')
                # run vasp jobs
                if not run_jobs('nsw0', output_dir):
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot run VASP single point calculations <<<'+'\n')
                    exit()
                # wait until all jobs are done
                while True:
                    fizzle_lostruns(3600)
                    if run_exists():
                        sleep(60)
                    else:
                        break
                # check launchpad status
                lp_state = check_lp(wfname_list)
                if lp_state == 'FIZZLED':
                    with open(log_file, 'a') as f:
                        f.write('>>> WARNING: Workflow is FIZZLED <<<'+'\n')
                elif lp_state == 'unknown':
                    with open(log_file, 'a') as f:
                        f.write('>>> Cannot proceed: the state of Workflow is not known <<<'+'\n')
                    exit()

                with open(log_file, 'a') as f:
                    f.write('FLAME-{}: VASP single point calculations ended'.format(step_number)+'\n')
                    f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'SP_calculations':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                exit()
            else:
                FLAME_step_name = 'data_collection'
###single point calculations collecting_data
        if FLAME_step_name == 'data_collection':
            with open(log_file, 'a') as f:
                f.write("-----------------------------------------------"+'\n')
                f.write('FLAME-{} data collection'.format(step_number)+'\n')
                f.write('start time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')
            # creating directories
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'task_files'))
            except FileExistsError:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: {} exists <<<'.format(os.path.join(Flame_dir,step_number,'task_files')+'\n'))
                exit()
            try:
                os.mkdir(os.path.join(Flame_dir,step_number,'excluded_task_files'))
            except FileExistsError:
                pass

            block_dir_list = find_block_folder()
            if block_dir_list:
                if len(block_dir_list) > 1:
                    with open(log_file, 'a') as f:
                        f.write('>>> ERROR: found more than one calculation directory <<<'+'\n')
                    exit()
                block_dir = block_dir_list[0]
            else:
                with open(log_file, 'a') as f:
                    f.write('>>> Cannot proceed: calculation directory was not found <<<'+'\n')
                exit()

            # VASP
            if input_list['ab_initio_code'] == 'VASP':

                with open(os.path.join(random_structures_dir,'min_epa.dat'), 'r') as f:
                    min_epa = float(f.readline().strip())
                next_step_minhocao_structs = []
                next_step_minhopp_clusters = [] 
                for root, dirs, files in os.walk(block_dir):
                    if 'task.json' in files:
                        try:
                            with open(os.path.join(root,'task.json'), 'r') as f:
                                a_task_file = json.loads(f.read())
                        except:
                            with open(log_file, 'a') as f:
                                f.write('cannot open task file in {}'.format(root)+'\n')
                            continue
                        task_label = a_task_file['task_label']
                        epa = a_task_file['output']['energy_per_atom']
                        if epa < min_epa:
                            min_epa = epa
                            with open(os.path.join(random_structures_dir,'min_epa.dat'), 'w') as f:
                                f.write(str(min_epa)) 
                        if epa < min_epa + 1.5 :
                            shutil.copyfile(os.path.join(root,'task.json'),\
                                            os.path.join(Flame_dir,step_number,'task_files',"task" + str(epa) + '.json'))
                            # select and store structures for next step minhocao
                            if 'bulk' in task_label:
                                struct = Structure.from_dict(a_task_file['output']['structure'])
                                forces = a_task_file['output']['forces']
                                stress = a_task_file['output']['stress']
                                tot_forces = []
                                for j in range(0,len(forces)):
                                    tot_forces.append(math.sqrt(forces[j][0]**2 + forces[j][1]**2 + forces[j][2]**2))
                                max_tot_foce = max(tot_forces)
                                external_pressure = (stress[0][0]+stress[1][1]+stress[2][2])/3
                                if max_tot_foce < 0.51:
                                    if external_pressure > -2 and external_pressure < 2:
                                        next_step_minhocao_structs.append(struct.as_dict())
                            if 'cluster' in task_label:
                                a_cluster = Structure.from_dict(a_task_file['output']['structure'])
                                forces = a_task_file['output']['forces']
                                tot_forces = []
                                for j in range(0,len(forces)):
                                    tot_forces.append(math.sqrt(forces[j][0]**2 + forces[j][1]**2 + forces[j][2]**2))
                                max_tot_foce = max(tot_forces)
                                if max_tot_foce < 1.01:
                                    lattice = [[input_list['box_size'],0,0],[0,input_list['box_size'],0],[0,0,input_list['box_size']]]
                                    n_cluster = Structure(lattice, a_cluster.species, a_cluster.cart_coords, coords_are_cartesian=True) 
                                    next_step_minhopp_clusters.append(n_cluster.as_dict())
                        else:
                            shutil.copyfile(os.path.join(root,'task.json'),\
                                            os.path.join(Flame_dir,step_number,'excluded_task_files',"task" + str(epa) + '.json'))

                if len(next_step_minhocao_structs) > 0:
                    with open(os.path.join(Flame_dir,step_number,'minhocao','next-step_bulk_structures.json'),'w') as f:
                        json.dump(next_step_minhocao_structs,f)
                with open(log_file, 'a') as f:
                    f.write('Number of structures for the next step minhocao: {}'.format(len(next_step_minhocao_structs))+'\n')

                if len(next_step_minhopp_clusters) > 0:
                    with open(os.path.join(Flame_dir,step_number,'minhopp','next-step_clusters.json'),'w') as f:
                        json.dump(next_step_minhopp_clusters,f)
                with open(log_file, 'a') as f:
                    f.write('Number of clusters for the next step minhopp: {}'.format(len(next_step_minhopp_clusters))+'\n')

                # mv/rm calculation files
                clean_after_run()    
                with open(log_file, 'a') as f:
                    f.write('VASP collecting data ended'+'\n')
                    f.write('end time: {}'.format(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))+'\n')

            # end of FLAME loop?
            if restart['FLAME_loop_stop'][0] == c_s_n and restart['FLAME_loop_stop'][1] == 'data_collection':
                with open(log_file, 'a') as f:
                    f.write('End of the training loop after {} cycles. Bye!'.format(c_s_n)+'\n')
                exit()
            else:
                FLAME_step_name = 'train'
