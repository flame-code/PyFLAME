import os 
import yaml
import json
import pymongo
from fireworks.fw_config import CONFIG_FILE_DIR

this_directory = os.path.abspath(os.path.dirname(__file__))
PyFLAME_directory = os.path.split(this_directory)[0]

with open(os.path.join(CONFIG_FILE_DIR,'db.json'), 'r') as f:
    d = json.loads(f.read())

myclient = pymongo.MongoClient(d['host'], username =  d['admin_user'], password = d['admin_password'])
mydb = myclient["path"]
mycol = mydb["directories"]
dirs = mycol.find()
run_dir = dirs[0]['run_dir']

db_dir = os.path.join(run_dir,'db')
additional_structures_dir = os.path.join(run_dir,'additional_structures')

output_dir = os.path.join(run_dir, 'output')

log_file = os.path.join(output_dir,'pyflame.log')
random_structures_dir = os.path.join(output_dir,'random_structures')
Flame_dir = os.path.join(output_dir,'FLAME_calculations')
debug_dir = os.path.join(output_dir,'debug')

with open(os.path.join(run_dir,'input.yaml'), 'r') as f:
    input_list = yaml.load(f, Loader=yaml.FullLoader)

with open(os.path.join(run_dir, 'config.yaml'), 'r') as f:
    config_list = yaml.load(f, Loader=yaml.FullLoader)

config_dir = config_list['CONFIG_DIR'] if config_list['CONFIG_DIR'] else CONFIG_FILE_DIR

steps_status = [True, True, True, True, True]

with open(os.path.join(run_dir,'restart.yaml'), 'r') as f:
    restart = yaml.load(f, Loader=yaml.FullLoader)

if restart['stop_after_step'] >= 0:
    steps_status[restart['stop_after_step']] = False
