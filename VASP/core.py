import os
import yaml
from fireworks import Workflow
from atomate.vasp.fireworks.core import OptimizeFW
from pymatgen.core.structure import Structure
from pymatgen.io.vasp.sets import MITRelaxSet
from VASP.fireworks import *
from workflows.config import *

if input_list['user_specified_VASP_files']:
    input_file_path = os.path.join(run_dir,'vasp_files')
else:
    input_file_path = os.path.join(PyFLAME_directory,'VASP','vasp_files')

with open(os.path.join(input_file_path,'INCAR.yaml'), 'r') as f:
    uis = yaml.load(f, Loader=yaml.FullLoader)
with open(os.path.join(input_file_path,'KPOINTS.yaml'), 'r') as f:
    uks = yaml.load(f, Loader=yaml.FullLoader)

def get_multistep_bulk_optimization_wf(structs, wf_name):
    vasp_fws = []
    priority = 3
    for a_struct in structs:
        opt1_fw = get_opt1_fw(priority, a_struct, uis, uks)
        vasp_fws.append(opt1_fw)

        opt2_fw = get_opt2_fw(priority, vasp_fws[-1], a_struct, uis, uks)
        vasp_fws.append(opt2_fw)

        bulk_fw = get_bulk_fw(priority,vasp_fws[-1], a_struct, uis, uks)
        vasp_fws.append(bulk_fw)

        scaled_bulk_fws = get_scaled_bulk_relax_fws(priority, vasp_fws[-1], a_struct, uis, uks)
        vasp_fws.extend(scaled_bulk_fws)

    wf = Workflow(vasp_fws, name = wf_name, metadata = None)
    return(wf)

def get_opt2_wf(structs, wf_name):
    vasp_fws = []
    priority = 2
    for a_struct in structs:
        opt1_fw = get_opt1_fw(priority, a_struct, uis, uks)
        vasp_fws.append(opt1_fw)

        opt2_fw = get_opt2_fw(priority, vasp_fws[-1], a_struct, uis, uks)
        vasp_fws.append(opt2_fw)

    wf = Workflow(vasp_fws, name = wf_name, metadata = None)
    return(wf)

def get_nsw0_wf(structs, wf_name, job_type):
    vasp_fws = []
    for a_struct in structs:
        nsw0_fw = get_nsw0_fw(job_type, a_struct, uis, uks)
        vasp_fws.append(nsw0_fw)
    wf = Workflow(vasp_fws, name = wf_name, metadata = None)
    return(wf)

def get_opt1_fw(priority, structure, uis, uks=None):
    user_incar_settings_opt1 = uis['opt1']
    user_incar_settings_opt1['LORBIT'] = None
    user_incar_settings_opt1['MAGMOM'] = None
    vis_relax_opt1 = MITRelaxSet(structure, user_incar_settings = user_incar_settings_opt1)
    if uks:
        v = vis_relax_opt1.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis_relax_opt1 = vis_relax_opt1.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'opt1')
    opt1_fw = RelaxFromScratchFW(structure = structure, vasp_input_set = vis_relax_opt1, name = fw_name, task_label = 'opt1', db_file = False, spec={'_priority': priority})
    return(opt1_fw)

def get_opt2_fw(priority, parents, structure, uis, uks=None):
    user_incar_settings_opt2 = uis['opt2']
    user_incar_settings_opt2['LORBIT'] = None
    user_incar_settings_opt2['MAGMOM'] = None
    vis_relax_opt2 = MITRelaxSet(structure, user_incar_settings = user_incar_settings_opt2)
    if uks:
        v = vis_relax_opt2.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis_relax_opt2 = vis_relax_opt2.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'opt2')
    opt2_fw = RelaxFromPrevFW(vasp_input_set = vis_relax_opt2, name = fw_name, task_label = 'opt2', db_file = False, parents = parents, spec = {'_allow_fizzled_parents': True, '_priority': priority})
    return(opt2_fw)

def get_bulk_fw(priority, parents, structure, uis, uks=None):
    user_incar_settings_bulk = uis['bulk']
    user_incar_settings_bulk['LORBIT'] = None
    user_incar_settings_bulk['MAGMOM'] = None
    vis_relax_bulk = MITRelaxSet(structure, user_incar_settings = user_incar_settings_bulk)
    if uks:
        v = vis_relax_bulk.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis_relax_bulk = vis_relax_bulk.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'bulk')
    bulk_fw = RelaxFromPrevFW(vasp_input_set = vis_relax_bulk, name = fw_name, task_label = 'bulk', db_file = False, parents = parents, spec = {'_allow_fizzled_parents': True, '_priority': priority})
    return(bulk_fw)
 
def get_scaled_bulk_relax_fws(priority, parents, structure, uis, uks=None):
    user_incar_settings_scaled_bulk = uis['opt2']
    user_incar_settings_scaled_bulk['LORBIT'] = None
    user_incar_settings_scaled_bulk['MAGMOM'] = None
    vis_relax_scaled_bulk = MITRelaxSet(structure, user_incar_settings = user_incar_settings_scaled_bulk)
    if uks:
        v = vis_relax_scaled_bulk.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis_relax_scaled_bulk = vis_relax_scaled_bulk.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'scaled_bulk')
    scaled_bulk_fws = []
    for s_f in (0.85, 0.90, 1.10, 1.20):
        s_b_fw = ScaledStructFW(vasp_input_set = vis_relax_scaled_bulk, scale_factor = s_f, name = fw_name, task_label = 'scaled_bulk', db_file = False, parents = parents, spec = {'_allow_fizzled_parents': True,  '_priority': priority})
        scaled_bulk_fws.append(s_b_fw)
    return(scaled_bulk_fws)

def get_nsw0_fw(job_type, structure, uis, uks=None):
    user_incar_settings_nsw0 = uis['nsw0']
    user_incar_settings_nsw0['LORBIT'] = None
    user_incar_settings_nsw0['MAGMOM'] = None
    if job_type == 'cluster':
        user_incar_settings_nsw0['KSPACING'] = 5
    vis_nsw0 = MITRelaxSet(structure, user_incar_settings = user_incar_settings_nsw0)
    if uks:
        v = vis_nsw0.as_dict()
        v.update({"user_kpoints_settings": uks})
        vis_nsw0 = vis_nsw0.__class__.from_dict(v)
    fw_name = "{}_{}-atom_{}".format(structure.composition.reduced_formula, len(structure), 'nsw0')
    nsw0_fw = RelaxFromScratchFW(structure = structure, vasp_input_set = vis_nsw0, name = fw_name, task_label = "{}".format('nsw0_'+job_type), db_file = False)
    return(nsw0_fw)
