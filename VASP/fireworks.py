import os 
from fireworks import Firework, PyTask
from atomate.vasp.firetasks.glue_tasks import CopyVaspOutputs
from atomate.vasp.firetasks.run_calc import RunVaspCustodian
from atomate.vasp.config import VASP_CMD
from atomate.vasp.firetasks.parse_outputs import VaspToDb
from atomate.common.firetasks.glue_tasks import PassCalcLocs
from atomate.vasp.firetasks.write_inputs import WriteVaspFromPMGObjects, WriteVaspFromIOSet
from custodian.vasp.handlers import VaspErrorHandler, FrozenJobErrorHandler
from workflows.config import *

handlers = [VaspErrorHandler(), FrozenJobErrorHandler(timeout=3600)]

class ScaledStructFW(Firework):
    def __init__(
        self,
        structure=None,
        scale_factor=None,
        name=None,
        task_label=None,
        vasp_input_set=None,
        vasp_cmd=VASP_CMD,
        prev_calc_loc=True,
        db_file=None,
        parents=None,
        **kwargs,
    ):
        t = []
        incar  = vasp_input_set.incar
        potcar = vasp_input_set.potcar
        fw_name = name

        if parents:
            t.append(CopyVaspOutputs(calc_loc=prev_calc_loc, contcar_to_poscar=True))
            t.append(PyTask(func='structure.core.scale_structure', args = [scale_factor]))
        elif structure:
            t.append(PyTask(func='structure.core.scale_structure', args = [scale_factor, structure]))
            t.append(WriteVaspFromPMGObjects(potcar=potcar))
        else:
            raise ValueError("Must specify structure or previous calculation")
        t.append(WriteVaspFromPMGObjects(incar=incar))
        t.append(RunVaspCustodian(vasp_cmd=vasp_cmd, auto_npar=">>auto_npar<<", handler_group = handlers, max_errors = 1))
        t.append(PassCalcLocs(name=name))
        t.append(VaspToDb(db_file=db_file, additional_fields={"task_label": task_label}))
        t.append(PyTask(func='workflows.core.extend_t', args = []))
        super().__init__(t, parents=parents, name=fw_name, **kwargs)

class RelaxFromPrevFW(Firework):
    def __init__(
        self,
        name=None,
        task_label=None,
        vasp_input_set=None,
        vasp_cmd=VASP_CMD,
        prev_calc_loc=True,
        db_file=None,
        parents=None,
        **kwargs,
    ):

        t = []
        incar = vasp_input_set.incar
        kpoints = vasp_input_set.kpoints or None
        fw_name = name

        if parents:
            t.append(CopyVaspOutputs(calc_loc=prev_calc_loc, contcar_to_poscar=True))
            t.append(WriteVaspFromPMGObjects(incar=incar))
            if kpoints:
                t.append(WriteVaspFromPMGObjects(kpoints=kpoints))
        else:
            raise ValueError("Must specify previous calculation")

        t.append(RunVaspCustodian(vasp_cmd=vasp_cmd, auto_npar=">>auto_npar<<", handler_group = handlers, max_errors = 3))
        t.append(PassCalcLocs(name=name))
        t.append(VaspToDb(db_file=db_file, additional_fields={"task_label": task_label}))
        t.append(PyTask(func='workflows.core.extend_t', args = []))
        super().__init__(t, parents=parents, name=fw_name, **kwargs)

class RelaxFromScratchFW(Firework):
    def __init__(
        self,
        structure,
        name=None,
        task_label=None,
        vasp_input_set=None,
        vasp_cmd=VASP_CMD,
        prev_calc_loc=True,
        db_file=None,
        parents=None,
        **kwargs,
    ):

        t = []
        incar  = vasp_input_set.incar
        potcar = vasp_input_set.potcar
        fw_name = name

        t.append(WriteVaspFromIOSet(structure=structure, vasp_input_set=vasp_input_set))
        t.append(RunVaspCustodian(vasp_cmd=vasp_cmd, auto_npar=">>auto_npar<<", handler_group = handlers, max_errors = 3))
        t.append(PassCalcLocs(name=name))
        t.append(VaspToDb(db_file=db_file, additional_fields={"task_label": task_label}))
        t.append(PyTask(func='workflows.core.extend_t', args = []))
        super().__init__(t, parents=parents, name= fw_name, **kwargs)
