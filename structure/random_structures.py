import os
import json
import re
import multiprocessing as mp
from collections import defaultdict
from pymatgen.core.composition import Composition
from pymatgen.core.structure import Structure
from pyxtal import pyxtal
from structure.core import get_allowed_n_atom_for_compositions
from structure.core import is_structure_valid
from workflows.config import *

def generate_random_bulk_natom(comp_list):
    pool = mp.Pool(mp.cpu_count())
    result_objects = []
    if input_list['check_density'] and os.path.exists(os.path.join(random_structures_dir,'densities.dat')):
        with open(os.path.join(random_structures_dir,'densities.dat'), 'r') as f:
            densities = [line.strip() for line in f]
    else:
        densities = None
    for a_comp in comp_list:
        elements = []
        n_elmnt = []
        for e, n in Composition(a_comp).items():
            elements.append(str(e))
            n_elmnt.append(int(n))
        allowed_n_atom = get_allowed_n_atom_for_compositions([a_comp])
        for a_n_a in allowed_n_atom:
            n_element = [n_el*int(a_n_a/sum(n_elmnt)) for n_el in n_elmnt]
            factor = 1.1 if a_n_a < 40 else 1.3
            for space_group in range(1,231):
                for attempt in range(30):
                    result_objects.append(pool.apply_async(generate_random_3d, args=(space_group, elements, n_element, factor, densities)))

    random_bulk_structures = []
    for r_o in result_objects:
        a_r_structure = r_o.get()
        if a_r_structure:
            random_bulk_structures.append(a_r_structure)

    pool.close()
    pool.join()

    store_pyxtal_results(random_bulk_structures)

def generate_random_3d(space_group, elements, n_element, factor, densities):
    rand_bulk = pyxtal()
    min_d_prefactor = input_list['min_distance_prefactor']
    try:
        rand_bulk.from_random(3, space_group, elements, n_element, factor)
    except:
        return(None)
    if densities:
        r_bulk = rand_bulk.to_pymatgen()
        if r_bulk.density.real < float(densities[0]):
            r_bulk.scale_lattice(r_bulk.volume * (r_bulk.density.real/float(densities[0])))
        if r_bulk.density.real > float(densities[1]):
            r_bulk.scale_lattice(r_bulk.volume * (r_bulk.density.real/float(densities[1])))

    if is_structure_valid(rand_bulk.to_pymatgen(), min_d_prefactor, False, input_list['check_density']):
        return(rand_bulk.to_pymatgen().as_dict())
    else:
        return(None)

def store_pyxtal_results(random_bulk_structures):
    random_bulk_structures_dict = defaultdict(list)
    for a_random_bulk_structure in random_bulk_structures:
        random_bulk_structures_dict[len(Structure.from_dict(a_random_bulk_structure))].append(a_random_bulk_structure)
    for keys in random_bulk_structures_dict.keys():
        if len(random_bulk_structures_dict[keys]) == 0:
            with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
                f.write('>>> WARNING: no random structure for structures with {} atoms is generated <<<'.format(str(keys))+'\n')
        else:
            with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
                f.write('{} random bulk structures with {} atoms are generated'.format(len(random_bulk_structures_dict[keys]), str(keys))+'\n')

    with open(os.path.join(random_structures_dir,'random_bulk_structures.json'),'w') as f:
        json.dump(random_bulk_structures_dict, f)
