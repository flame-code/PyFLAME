import os
import sys
import yaml
from fireworks import Firework, PyTask

class RandomStructGenPyXtalFW(Firework):
    def __init__(
       self,
       compositions,
       name,
       parents=None,
       **kwargs
    ):
        t = []
        t.append(PyTask(func='structure.random_structures.generate_random_bulk_natom', args = [compositions]))
        super().__init__(t, name = name, **kwargs)
