import os
import json
from collections import defaultdict
from fireworks import Workflow
from pymatgen.core.composition import Composition
from pymatgen.core.structure import Structure
from structure.core import get_allowed_n_atom_for_compositions
from structure.fireworks import RandomStructGenPyXtalFW
from workflows.config import *

def get_pyxtal_wf(comp_list):
    wf_name = 'step-1'
    pyxtal_fws = []
    pyxtalfw = RandomStructGenPyXtalFW(comp_list, name='pyxtal')
    pyxtal_fws.append(pyxtalfw)
    pyxtal_wf = Workflow(pyxtal_fws, name = wf_name)
    return pyxtal_wf

