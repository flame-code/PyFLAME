import os 
import yaml
import json
import numpy as np
from random import sample, shuffle
from collections import defaultdict
from pymatgen.core.structure import Structure
from pymatgen.core.composition import Composition
from pymatgen.ext.matproj import MPRester
from pymatgen.analysis.molecule_structure_comparator import CovalentRadius
from pymatgen.core.periodic_table import Element
from pymatgen.io.vasp import Poscar
from workflows.config import *

def is_structure_valid(structure, min_d_prefactor, X, density):
    if density and os.path.exists(os.path.join(random_structures_dir,'densities.dat')):
        with open(os.path.join(random_structures_dir,'densities.dat'), 'r') as f:
            densitie = [line.strip() for line in f]
        if round(structure.density.real, 3) < round(float(densitie[0])*0.90, 3) or\
           round(structure.density.real, 3) > round(float(densitie[1])*1.10, 3):
            return(False)

    d_matrix = structure.distance_matrix
    for isites in range(len(structure)):
        if not structure.get_neighbors(structure[isites], 5):
            return(False)
        for jsites in range(len(structure)):
            min_d = get_min_d(structure[isites].species_string, structure[jsites].species_string, X) * min_d_prefactor
            if d_matrix[isites,jsites] > 0 and d_matrix[isites,jsites] < min_d:
                return(False)
    return(True)

def get_min_d(elmnt1, elmnt2, X):
    covalent_radius = CovalentRadius.radius
    min_d = (covalent_radius[elmnt1] + covalent_radius[elmnt2])
    x_prefactor = 1
    if X:
        try:
            x_prefactor = 1 - (abs(Element(elmnt1).X - Element(elmnt2).X)/(3.98 - 0.79)) * 0.2
        except:
            pass
    return(min_d * x_prefactor)

def r_cut():
    all_rcuts = []
    n_atom_in_rcut = input_list['n_atom_in_rcut']
    file_name =  os.path.join(random_structures_dir,'optimized_random_bulk_structures.json')
    with open(file_name, 'r') as f:
        all_structs=json.loads(f.read())
    for struct in all_structs:
        for d in range(6,30):
            if len(Structure.from_dict(struct).get_sites_in_sphere([0,0,0],d)) < n_atom_in_rcut:
                continue
            else:
                all_rcuts.append(d)
                break
    return sum(all_rcuts)/len(all_rcuts)

def scale_structure(scale_factor, struct = None):
    if not struct:
        struct = Structure.from_file('./POSCAR')
    struct.scale_lattice(struct.volume*scale_factor)
    Poscar(struct).write_file('POSCAR')

def get_compositions_elements():
    if len(input_list['Chemical_formula']) > 0:
        composition_list = input_list['Chemical_formula']
        element_list = []
        for a_composition in composition_list:
            for elmnt in Composition(a_composition).elements:
                    if str(elmnt) not in element_list:
                        element_list.append(str(elmnt)) 
        return(composition_list, element_list)
    else:
        return None, None

def get_known_structures(composition_list):
    mpr= MPRester()
    k_s = []
    for a_composition in composition_list:
        k_s.extend(mpr.get_structures(a_composition))
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('Number of known structures in the MPDB for {}: {}'.format(a_composition, len(mpr.get_structures(a_composition)))+'\n')
        densities = []
        for a_k_s in k_s:
            densities.append(a_k_s.density.real)
        minmaxdensity = [min(densities), max(densities)]
        with open(os.path.join(random_structures_dir,'densities.dat'), 'w') as f:
            f.writelines(["%s\n" % dens  for dens in minmaxdensity])
    todump = []
    for struct in k_s:
        if len(struct.sites) in input_list['bulk_number_of_atoms']:
            todump.append(struct.as_dict())
    if len(todump) == 0:
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('>>> WARNING: No bulk structure with given number of atoms was found in the MPDB <<<'+'\n')
    else:
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('Number of known structures in the MPDB with given number of atoms:{}'.format(len(todump))+'\n')
        with open(os.path.join(random_structures_dir,'known_bulk_structures.json'),'w') as f:
            json.dump(todump, f)

def read_bulk_structure_from_db(composition_list, file_format, anonymous_formula):
    with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
        f.write('Reading structures from the local database'+'\n')
    d_min_prefactor = input_list['min_distance_prefactor']
    db_bulk_structures_dict = defaultdict(list)
    for a_comp in composition_list:
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write('Composition: {}'.format(a_comp)+'\n')
        n_elmnt = []
        element_list = []
        for e, n in Composition(a_comp).items():
            element_list.extend(int(n) * [str(e)])
            n_elmnt.append(int(n))
        allowed_n_atom = get_allowed_n_atom_for_compositions([a_comp])
        if 'json' in file_format:
            for a_file in os.listdir(db_dir):
                if a_file.endswith('json'): 
                    with open(os.path.join(db_dir,a_file), 'r') as f:
                        db_structures = json.loads(f.read())
                    for a_db_structure_dict in db_structures:
                        a_db_structure = Structure.from_dict(a_db_structure_dict)
                        if len(a_db_structure) in allowed_n_atom:
                            if anonymous_formula:
                                elements = int(len(a_db_structure)/sum(n_elmnt)) * element_list
                                shuffle(elements)
                                for i in range(len(a_db_structure.sites)):
                                    a_db_structure.replace(i, elements[i])
                            if a_db_structure.composition.reduced_formula == Composition(a_comp).reduced_formula\
                                                        and is_structure_valid(a_db_structure, d_min_prefactor, False, input_list['check_density']):
                                db_bulk_structures_dict[len(a_db_structure)].append(a_db_structure.as_dict())
        if 'vasp' in file_format:
            for a_file in os.listdir(db_dir):
                if a_file.endswith('vasp'):
                   a_db_structure = Structure.from_file(os.path.join(db_dir,a_file))
                   if len(a_db_structure) in allowed_n_atom:
                       if anonymous_formula:
                           elements = int(len(a_db_structure)/sum(n_elmnt)) * element_list
                           shuffle(elements)
                           for i in range(len(a_db_structure.sites)):
                               a_db_structure.replace(i, elements[i])
                       if a_db_structure.composition.reduced_formula == Composition(a_comp).reduced_formula\
                                                    and is_structure_valid(a_db_structure, d_min_prefactor, False, input_list['check_density']):
                           db_bulk_structures_dict[len(a_db_structure)].append(a_db_structure.as_dict())

    for keys in db_bulk_structures_dict.keys():
        with open(os.path.join(random_structures_dir,'random_structure.dat'), 'a') as f:
            f.write("{} bulk structures for {}-atom systems are retrived from the local db.".format(len(db_bulk_structures_dict[keys]), keys)+'\n')
    return db_bulk_structures_dict

def get_allowed_n_atom_for_compositions(composition_list):
    allowed_n_atom = []
    for a_comp in composition_list:
        n_elmnt = []
        for e, n in Composition(a_comp).items():
            n_elmnt.append(int(n))
        n_element = [n_el for n_el in n_elmnt]
        nmax = 1
        while sum(n_element) <= max(input_list['bulk_number_of_atoms']):
            if sum(n_element) >= min(input_list['bulk_number_of_atoms']) and sum(n_element) not in allowed_n_atom\
                                                             and sum(n_element) in input_list['bulk_number_of_atoms']:
                allowed_n_atom.append(sum(n_element))
            nmax = nmax + 1
            n_element = [n_el * nmax for n_el in n_elmnt]
    return(allowed_n_atom)

def write_compositions(composition_list):
    with open(os.path.join(random_structures_dir,'compositions.dat'), 'w') as f:
        f.writelines(["%s\n" % comp  for comp in composition_list])

def read_composition_list():
    if os.path.exists(os.path.join(random_structures_dir,'compositions.dat')): 
        with open(os.path.join(random_structures_dir,'compositions.dat'), 'r') as f:
            composition_list = [line.strip() for line in f]
    else:
        return None
    if len(composition_list) > 0:
        return composition_list
    else:
        return None

def write_elements(element_list):
    with open(os.path.join(random_structures_dir,'elements.dat'), 'w') as f:
        f.writelines(["%s\n" % el  for el in element_list])

def read_element_list():
    with open(os.path.join(random_structures_dir,'elements.dat'), 'r') as f:
        element_list = [line.strip() for line in f]
    return element_list 
